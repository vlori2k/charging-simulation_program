# Charging-Simulation_Program

A Charging-Simulation program for Electric Vehicles
- Different Electric Vehicles arrives to different districts followed by CDF distribution
- Each district is assigned to a distribution(some districts has two)
- If all chargers + the queue is full, EV gets rejected from the station and move to find another
- Each EV has 2 attempts. If EV has no more attempts, then it gets "Dejected"
- Number of chargers currently in use impacts the charging rate of the station.

