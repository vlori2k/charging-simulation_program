import random
from termcolor import colored
from import_data_from_csv_into_simulation import district_names
from import_data_from_csv_into_simulation import charge_points_schuko, charge_points_type2, charge_points_superCharger, regions

from charging_modes import type1charger, type2charger, type3charger




class ChargingStation():
    total_visited_EVs = 0
    total_EVs_entered_into_station = 0
    rejected_EVs = 0
    reneged_EVs = 0
    mission_completed = False



    def __init__(self, station_name):
        self.station_name = station_name

        self.station_index = district_names.index(self.station_name)  # imported from the oslo_stations_and_chargers.
        print(f"Station Index: {self.station_index}")
        print(f"Station Name: {self.station_name}")


        self.type_1_charger = charge_points_schuko[self.station_index]  # imported from the import_data_from_csv_into_simulation.py
        self.type_2_charger = charge_points_type2[self.station_index]  # imported from the import_data_from_csv_into_simulation.py
        self.super_charger = charge_points_superCharger[self.station_index]  # imported from the import_data_from_csv_into_simulation.py
        self.regions = regions[self.station_index]



        print(f"type_1_chargers: {self.type_1_charger}")
        print(f"type_2_chargers: {self.type_2_charger}")
        print(f"type_3_chargers: {self.super_charger}")
        print(f"Region: {self.regions}")
        print("---------------------------------------")


        self.my_neighbors = []
        self.total_amount_of_chargers = []
        self.dejected_EVs = []


        # ==================   Send information about amount of chargers into charging_modes.py ===============#
        self.type_1_all_chargers = []

        for i in range(0, self.type_1_charger):
            self.type_1_all_chargers.append(type1charger())
            self.total_amount_of_chargers.append(type1charger())
        # ============================================================================================#
        self.type_2_all_chargers = []

        for i in range(0, self.type_2_charger):
            self.type_2_all_chargers.append(type2charger())
            self.total_amount_of_chargers.append(type2charger())

        # ============================================================================================#
        self.type_3_all_chargers = []

        for i in range(0, self.super_charger):
            self.type_3_all_chargers.append(type3charger())
            self.total_amount_of_chargers.append(type3charger())

        self.price_of_building_a_new_charger = 60000 # price in NOK

        self.use_this_list_to_hold_current_amount_of_new_type1_chargers_added = []
        self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added = []

        #self.budget_for_station = 9000000 # budget for each station
        self.budget_for_station = 0

        self.remaining_money_left = 0

        self.amount_of_chargers_available_to_build_with_the_budget = round(self.budget_for_station / self.price_of_building_a_new_charger)

    # ===================================Queue parameters here:==================
        self.EVs_waiting_in_queue = []
        self.max_queue_length = 20
    # ===========================================================================

    def __eq__(self, other):
        return self.station_index == other.station_index

    def __repr__(self):
        return (f"""
        Station: {self.station_name}: 
        Total amount of chargers at this station: {len(self.total_amount_of_chargers)}   

        Amount of type1 chargers: {self.type_1_charger}
        Amount of type2: {self.type_2_charger}
        Amount of superchargers: {self.super_charger} 

         """)

    def add_car_from_distribution(self, car):
        name_of_first_station = self.station_name
        car.first_station_visited.append(name_of_first_station)
        #print("EV got distributed to station:{}".format(car.first_station_visited) + "{}".format(car.model) + " -- {} ".format(car.attempts) + "attempts left")
        self.total_visited_EVs += 1
        car_added = self.always_pick_the_best_charger_if_possible(car)

        if car_added == True:
            self.total_EVs_entered_into_station += 1
        else:
            try:
                queue_car = self.EVs_waiting_in_queue.pop(0)
                self.always_pick_the_best_charger_if_possible(queue_car)

            except IndexError as e:
                #print("Calm before the storm..")
                print()


    def add_car_to_neighbor_station(self, car):
        #print("EV visited neighbor station! {} ".format(car.model) + " -- {} ".format(car.attempts) + "attempts left")
        self.total_visited_EVs += 1
        car_added_to_neighbor_station = self.always_pick_the_best_charger_if_possible(car)

        if car_added_to_neighbor_station == True:
            #print("EV entered neighbor station!")
            self.total_EVs_entered_into_station += 1
        else:
            try:
                queue_car = self.EVs_waiting_in_queue.pop(0)

                self.always_pick_the_best_charger_if_possible(queue_car)
            except IndexError as e:
                #print("Waiting for EV`s to arrive from other stations")
                print()


    def add_neighbor(self, n):
        self.my_neighbors.append(n)


    def show_neighbors(self):
        for neighbors in self.my_neighbors:
            print(neighbors)


    def add_to_waiting_queue(self, car):
        if len(self.EVs_waiting_in_queue) >= self.max_queue_length:

            self.send_car_to_next_station(car)
            self.rejected_EVs += 1

        elif len(self.EVs_waiting_in_queue) >= 0 and len(self.total_amount_of_chargers) == 0:

            self.send_car_to_next_station(car)
            self.rejected_EVs = self.rejected_EVs + 1

        else:
            self.EVs_waiting_in_queue.append(car)
            return True

    def update_patience_and_status_for_EVs_in_queue(self):
        self.EVs_that_wants_to_leave_the_queue_and_go_to_next_station = []
        for EVs in self.EVs_waiting_in_queue:
            EVs.update_patience_in_queue()
            if EVs.zero_patience_car_wants_to_leave == True:
                self.EVs_that_wants_to_leave_the_queue_and_go_to_next_station.append(EVs)
                self.EVs_waiting_in_queue.remove(EVs)
                self.send_car_to_next_station(self.EVs_that_wants_to_leave_the_queue_and_go_to_next_station.pop(0))
                self.reneged_EVs = self.reneged_EVs + 1


    def always_pick_the_best_charger_if_possible(self, car):
        #print(colored("Let us see if Type 3 chargers are available...", 'cyan'))
        print()
        for type3charger in self.type_3_all_chargers:
            if type3charger.car == None:
                type3charger.add_car_to_charger(car)
                #print(colored("TYPE 3 was available, picked it!", 'cyan'))
                print()
                return True
        else:
            #print(colored("No type 3 chargers available, what about type 2?", 'cyan'))
            print()
            return self.pick_the_second_best_charger(car)

    def pick_the_second_best_charger(self, car):
        #print(colored("Let us see if Type 2 chargers are available...", 'cyan'))
        print()
        for type2charger in self.type_2_all_chargers:
            if type2charger.car == None:
                type2charger.add_car_to_charger(car)
                #print(colored("TYPE 2 was available, picked it!", 'cyan'))
                print()
                return True
        else:
            #print(colored("No type 2 chargers avaiable, what about type 1/Schkuko then?",'cyan'))
            print()
            return self.pick_a_schuko_charger(car)

    def pick_a_schuko_charger(self, car):
        #print(colored("Ok..Let us see if Type 1/Schkuko chargers are available...", 'cyan'))
        print()
        for type1charger in self.type_1_all_chargers:
            if type1charger.car == None:
                type1charger.add_car_to_charger(car)
                #print(colored("TYPE 1/Schkuko was available, picked it!",'cyan'))
                print()
                return True
        else:
            #print("All chargers are full! --> plz wait in queue!")
            print()
            return self.add_to_waiting_queue(car)


    def grab_car_from_waiting_queue_and_place_to_charger(self):
        for charger3 in self.type_3_all_chargers:
            if len(self.EVs_waiting_in_queue) > 0 and charger3.car == None:
                try:
                    queued_car = self.EVs_waiting_in_queue.pop(0)
                    charger3.add_car_to_charger(queued_car)

                    #print(colored(f"Grabbed car from queue and placed to Charger 3: {queued_car}", 'magenta'))
                    print()
                    print()
                except IndexError as e:
                    #print("Queue is empty")
                    print()


        for charger2 in self.type_2_all_chargers:
            if len(self.EVs_waiting_in_queue) > 0 and charger2.car == None:
                try:
                    queued_car = self.EVs_waiting_in_queue.pop(0)
                    charger2.add_car_to_charger(queued_car)

                    #print(colored(f"Grabbed car from queue and placed to Charger 2: {queued_car} ", 'magenta'))
                    print()
                    print()
                except IndexError as e:

                    #print("Queue is empty")
                    print()


        for charger1 in self.type_1_all_chargers:
            if len(self.EVs_waiting_in_queue) > 0 and charger1.car == None:
                try:
                    queued_car = self.EVs_waiting_in_queue.pop(0)
                    charger1.add_car_to_charger(queued_car)
                    #print(colored(f"Grabbed car from queue and placed to Charger 1: {queued_car} ", 'magenta'))
                    print()
                    print()

                except IndexError as e:

                    #print("Queue is empty")
                    print()

    def add_1_more_chargers_of_type1_into_this_station(self):
            for add_more_type1_charger_into_the_station in range(0, 1):
                self.use_this_list_to_hold_current_amount_of_new_type1_chargers_added.append(type1charger())
                self.type_1_all_chargers.append(type1charger())
                self.total_amount_of_chargers.append(type1charger())
            return True


    def add_2_more_chargers_of_type1_into_this_station(self):
        for add_more_type1_charger_into_the_station in range(0, 2):
            self.use_this_list_to_hold_current_amount_of_new_type1_chargers_added.append(type1charger())
            self.type_1_all_chargers.append(type1charger())
            self.total_amount_of_chargers.append(type1charger())


    def add_1_more_chargers_of_type2_into_this_station(self):
        for add_more_type2_chargers_into_the_station in range(0, 1):
            self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added.append(type2charger())
            self.type_2_all_chargers.append(type2charger())
            self.total_amount_of_chargers.append(type2charger())

    def add_2_more_chargers_of_type2_into_this_station(self):
        for add_more_type2_chargers_into_the_station in range(0, 2):
            self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added.append(type2charger())
            self.type_2_all_chargers.append(type2charger())
            self.total_amount_of_chargers.append(type2charger())

    def add_3_more_chargers_of_type2_into_this_station(self):
        for add_more_type2_chargers_into_the_station in range(0, 3):
            self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added.append(type2charger())
            self.type_2_all_chargers.append(type2charger())
            self.total_amount_of_chargers.append(type2charger())

    def add_4_more_chargers_of_type2_into_this_station(self):
        for add_more_type2_chargers_into_the_station in range(0, 4):
            self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added.append(type2charger())
            self.type_2_all_chargers.append(type2charger())
            self.total_amount_of_chargers.append(type2charger())

    def add_6_more_chargers_of_type2_into_this_station(self):
        for add_more_type2_chargers_into_the_station in range(0, 6):
            self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added.append(add_more_type2_chargers_into_the_station)
            self.type_2_all_chargers.append(type2charger())
            self.total_amount_of_chargers.append(type2charger())

    def add_31_more_chargers_of_type2_into_this_station(self):
        for add_more_type2_chargers_into_the_station in range(0, 31):
            self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added.append(add_more_type2_chargers_into_the_station)
            self.type_2_all_chargers.append(type2charger())
            self.total_amount_of_chargers.append(type2charger())

    def add_15_more_chargers_of_type2_into_this_station(self):
        for add_more_type2_chargers_into_the_station in range(0, 15):
            self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added.append(add_more_type2_chargers_into_the_station)
            self.type_2_all_chargers.append(type2charger())
            self.total_amount_of_chargers.append(type2charger())





    def add_500_more_chargers_of_type2_into_this_station(self):
        for add_more_type2_chargers_into_the_station in range(0, 500):
            self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added.append(add_more_type2_chargers_into_the_station)
            self.type_2_all_chargers.append(type2charger())
            self.total_amount_of_chargers.append(type2charger())



    def give_me_neighbor_stations(self):
        for neighbor in self.my_neighbors:
            print("Closest nearest station are: {} ".format(neighbor))




    def pick_neighbor_with_lowest_queue(self):
        queue_length_at_stations = 21
        low_queue_length = None
        for neighbor_stations in self.my_neighbors:
            EVs_that_are_waiting_in_queue = len(neighbor_stations.EVs_waiting_in_queue)
            if EVs_that_are_waiting_in_queue < queue_length_at_stations:
                low_queue_length = neighbor_stations
                queue_length_at_stations = EVs_that_are_waiting_in_queue
        return low_queue_length

    def pick_neighbor_with_most_idle_chargers(self):
        max_percentage_chargers_used_at_other_stations = 101
        most_idle_chargers = None
        for neighbor_stations in self.my_neighbors:
            percentage_of_chargers_being_used = neighbor_stations.percentage_of_chargers_in_use
            if percentage_of_chargers_being_used < max_percentage_chargers_used_at_other_stations:
                most_idle_chargers = neighbor_stations
                max_percentage_chargers_used_at_other_stations = percentage_of_chargers_being_used
        return most_idle_chargers



    def pick_a_random_neighbor(self):
        return random.choice(self.my_neighbors)


    def send_car_to_next_station(self, car):
        if not car.decrease_attempts():
            self.dejected_EVs.append(car)
            #print("Car added to dejected list! {} ".format(car))
            print()

        else:
            #print("Sending car to next station! {} ".format(car))
            car.reset_queue_settings()
            #self.pick_neighbor_with_most_idle_chargers().add_car_to_neighbor_station(car)
            self.pick_a_random_neighbor().add_car_to_neighbor_station(car)
            #self.pick_neighbor_with_lowest_queue().add_car_to_neighbor_station(car)
            #print("-----------------------------------------")
            print()


    def send_car_to_next_station2(self, car): # in case you want to use
        if not car.decrease_attempts():
            self.dejected_EVs.append(car)
            #print("Car added to dejected list! {} ".format(car))
            print()

        else:

            print("Sending car to next station! {} ".format(car))
            self.pick_a_random_neighbor().add_car_to_neighbor_station(car)
            new_neighbor = self.pick_a_random_neighbor()
            picked_neighbors = []
            while new_neighbor in car.stations_visited:
                new_neighbor = random.choice(self.my_neighbors)
                picked_neighbors.append(new_neighbor)
                print("Looking for new unvisited neighbor {} ".format(car.model) + " I still have:{} ".format(car.attempts) + " attempts left")
                if len(picked_neighbors) == len(self.my_neighbors):
                    print("No new unvisited neighbors, returning back to primary station {}".format(car.model))
                    self.send_car_to_primary_station(car)
                    return
            car.stations_visited.append(new_neighbor)
            car.reset_queue_settings()
            new_neighbor.add_car_to_neighbor_station(car)

            print("-----------------------------------------")
            print()

    def send_car_to_primary_station(self, car):
        for first_primary_station in car.first_station_visited:
            if first_primary_station == self.station_name:
                return self.add_car_from_distribution(car)



    def reset_method(self):
        self.total_visited_EVs = 0
        self.total_EVs_entered_into_station = 0

        self.station_index = 0
        self.received_EVs_from_neighbors = 0

        self.rejected_EVs = 0

        self.reneged_EVs = 0
        self.dejected_EVs = []

        self.EVs_waiting_in_queue = []
        self.EVs_that_wants_to_leave_the_queue_and_go_to_next_station = []

        self.amount_of_EVs_charging_at_type1 = 0
        self.amount_of_EVs_charging_at_type2 = 0
        self.amount_of_EVs_charging_at_type3 = 0
        self.total_chargers_in_use = 0

        self.total_amount_of_EVs_finished_charging_at_type1 = 0
        self.total_amount_of_EVs_finished_charging_at_type2 = 0
        self.total_amount_of_EVs_finished_charging_at_type3 = 0

        for charger1 in self.type_1_all_chargers:
            charger1.finished_charging_at_type1 = 0
            charger1.car = None
            charger1.active_car = False

        for charger2 in self.type_2_all_chargers:
            charger2.finished_charging_at_type2 = 0
            charger2.car = None
            charger2.active_car = False

        for charger3 in self.type_3_all_chargers:
            charger3.finished_charging_at_type3 = 0
            charger3.car = None
            charger3.active_car = False

    def print_status_of_station(self):
        self.amount_of_EVs_charging_at_type1 = 0
        self.amount_of_EVs_charging_at_type2 = 0
        self.amount_of_EVs_charging_at_type3 = 0

        self.total_amount_of_EVs_finished_charging_at_type1 = 0
        self.total_amount_of_EVs_finished_charging_at_type2 = 0
        self.total_amount_of_EVs_finished_charging_at_type3 = 0

        for charger1 in self.type_1_all_chargers:
            if charger1.car:
                self.amount_of_EVs_charging_at_type1 += 1
            self.total_amount_of_EVs_finished_charging_at_type1 += charger1.finished_charging_at_type1

        for charger2 in self.type_2_all_chargers:
            if charger2.car:
                self.amount_of_EVs_charging_at_type2 += 1
            self.total_amount_of_EVs_finished_charging_at_type2 += charger2.finished_charging_at_type2

        for charger3 in self.type_3_all_chargers:
            if charger3.car:
                self.amount_of_EVs_charging_at_type3 += 1
            self.total_amount_of_EVs_finished_charging_at_type3 += charger3.finished_charging_at_type3

        self.total_chargers_in_use = (self.amount_of_EVs_charging_at_type1 + self.amount_of_EVs_charging_at_type2 + self.amount_of_EVs_charging_at_type3)

        self.percentage_of_chargers_in_use = self.total_chargers_in_use / len(self.total_amount_of_chargers) * 100 if len(self.total_amount_of_chargers) > 0 else 0

        self.total_EVs_in_station = (self.total_chargers_in_use + len(self.EVs_waiting_in_queue))

        self.total_finished_EVs_charging = (self.total_amount_of_EVs_finished_charging_at_type1 + self.total_amount_of_EVs_finished_charging_at_type2 + self.total_amount_of_EVs_finished_charging_at_type3)

        self.total_amount_of_cost_of_placing_new_chargers_at_this_station = (len(self.use_this_list_to_hold_current_amount_of_new_type1_chargers_added) + len(self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added)) * self.price_of_building_a_new_charger
        self.remaining_money_left = self.budget_for_station - self.total_amount_of_cost_of_placing_new_chargers_at_this_station

        return (f"""             
Total amount of EV that visited the station during the day : {self.total_visited_EVs}.
EV`s that got to enter the station     : {self.total_EVs_entered_into_station} 

"--------------------------------------------------------------------------------------------------------------------------------"
EVs charging at type 1: {self.amount_of_EVs_charging_at_type1}
Extra type1 chargers added: {(len(self.use_this_list_to_hold_current_amount_of_new_type1_chargers_added))}
EVs charging at type 2: {self.amount_of_EVs_charging_at_type2}
Extra type2 chargers added: {(len(self.use_this_list_to_hold_current_amount_of_new_type2_chargers_added))}
EVs charging at type 3: {self.amount_of_EVs_charging_at_type3}

EVs charging totally  : {self.total_chargers_in_use}
Percentage of chargers used : {self.percentage_of_chargers_in_use}%

Cars currently waiting in Queue     : {(len(self.EVs_waiting_in_queue))}
Total EVs currently in system      : {self.total_EVs_in_station}

"--------------------------------------------------------------------------------------------------------------------------------"
EV`s finished charging at type 1       : {self.total_amount_of_EVs_finished_charging_at_type1}
EV`s finished charging at type 2       : {self.total_amount_of_EVs_finished_charging_at_type2}
EV`s finished charging at type 3       : {self.total_amount_of_EVs_finished_charging_at_type3}
Total amount of finished charging      : {self.total_finished_EVs_charging}

Total EVs rejected from the station: {self.rejected_EVs}

Total EVs that have bailed away    : {self.reneged_EVs}

Cars dejected after 2 attempts     : {len(self.dejected_EVs)}
Cost of adding new chargers at this station: {self.total_amount_of_cost_of_placing_new_chargers_at_this_station}
Remaining money left:                        {self.remaining_money_left}


"--------------------------------------------------------------------------------------------------------------------------------"
""")