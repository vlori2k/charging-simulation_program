import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

read_CSV_stats_per_day_for_OSLO = pd.read_csv('results_per_day/ALL_stations_together.csv', sep=";", encoding ="ISO-8859-1")
read_day_column = read_CSV_stats_per_day_for_OSLO[read_CSV_stats_per_day_for_OSLO['day'] == 0]



def plot_results_about_oslo_north_stations():

    north = read_day_column[read_day_column['Region'] == 'north']


    top4_visited_stations = north.nlargest(15, 'total_visited_EVs')

    dataframe_for_oslo_north_stations = read_CSV_stats_per_day_for_OSLO[read_CSV_stats_per_day_for_OSLO['name'].isin(top4_visited_stations['name'])]


    sns.relplot(x='day', y='avg_queue_length', data=dataframe_for_oslo_north_stations, hue='name',kind="line")
    plt.suptitle("North Oslo")


    sns.relplot(x='day', y='avg_total_EV_in_station', data=dataframe_for_oslo_north_stations, hue='name', kind="line")
    plt.suptitle("North Oslo")

    sns.relplot(x='day', y='total_rejected_EVs', data=dataframe_for_oslo_north_stations, hue='name', kind="line")
    plt.suptitle("North Oslo")


    sns.relplot(x='day', y='total_dejected_EVs', data=dataframe_for_oslo_north_stations, hue='name',kind="line")
    plt.suptitle("North Oslo")


    plt.show()


def plot_results_about_oslo_north_east_stations():

    north_east = read_day_column[read_day_column['Region'] == 'north_east']

    top4_visited_stations = north_east.nlargest(15,'total_visited_EVs')

    dataframes_for_oslo_north_east_stations = read_CSV_stats_per_day_for_OSLO[read_CSV_stats_per_day_for_OSLO['name'].isin(top4_visited_stations['name'])]

    sns.relplot(x='day', y='avg_queue_length', data=dataframes_for_oslo_north_east_stations, hue='name',kind="line")
    plt.suptitle("North-east Oslo")


    sns.relplot(x='day', y='avg_total_EV_in_station', data=dataframes_for_oslo_north_east_stations, hue='name', kind="line")
    plt.suptitle("North-east Oslo")

    sns.relplot(x='day', y='total_rejected_EVs', data=dataframes_for_oslo_north_east_stations, hue='name',kind="line")
    plt.suptitle("North-east Oslo")


    sns.relplot(x='day', y='total_dejected_EVs', data=dataframes_for_oslo_north_east_stations, hue='name',kind="line")
    plt.suptitle("North-east Oslo")

    plt.show()



def plot_results_about_oslo_centrum_middle_stations():

    centrum_middle = read_day_column[read_day_column['Region'] == 'centrum_middle']

    top4_visited_stations = centrum_middle.nlargest(15, 'total_visited_EVs')

    dataframes_for_oslo_centrum_middle_stations = read_CSV_stats_per_day_for_OSLO[read_CSV_stats_per_day_for_OSLO['name'].isin(top4_visited_stations['name'])]

    sns.relplot(x='day', y='avg_queue_length', data=dataframes_for_oslo_centrum_middle_stations, hue='name', kind='line')
    plt.suptitle("Centrum-middle Oslo")

    sns.relplot(x='day', y='avg_total_EV_in_station', data=dataframes_for_oslo_centrum_middle_stations, hue='name', kind='line')
    plt.suptitle("Centrum-middle Oslo")

    sns.relplot(x='day', y='total_rejected_EVs', data=dataframes_for_oslo_centrum_middle_stations, hue='name', kind='line')
    plt.suptitle("Centrum-middle Oslo")

    sns.relplot(x='day', y='total_dejected_EVs', data=dataframes_for_oslo_centrum_middle_stations, hue='name', kind='line')
    plt.suptitle("Centrum-middle Oslo")

    plt.show()




def plot_results_about_oslo_north_west_stations():
    north_west = read_day_column[read_day_column['Region'] == 'north_west']

    top4_visited_stations = north_west.nlargest(15,'total_visited_EVs')

    dataframes_for_oslo_north_west_stations = read_CSV_stats_per_day_for_OSLO[read_CSV_stats_per_day_for_OSLO['name'].isin(top4_visited_stations['name'])]

    sns.relplot(x='day', y='avg_queue_length', data=dataframes_for_oslo_north_west_stations, hue='name', kind='line')
    plt.suptitle("North-west Oslo")


    sns.relplot(x='day', y='avg_total_EV_in_station', data=dataframes_for_oslo_north_west_stations, hue='name', kind='line')
    plt.suptitle("North-west Oslo")

    sns.relplot(x='day', y='total_rejected_EVs', data=dataframes_for_oslo_north_west_stations, hue='name',kind='line')
    plt.suptitle("North-west Oslo")

    sns.relplot(x='day', y='total_dejected_EVs', data=dataframes_for_oslo_north_west_stations, hue='name', kind='line')
    plt.suptitle("North-west Oslo")

    plt.show()

def plot_results_about_oslo_west_stations():
    west = read_day_column[read_day_column['Region'] == 'west']

    top4_visited_stations = west.nlargest(15, 'total_visited_EVs')

    dataframes_for_oslo_west_stations = read_CSV_stats_per_day_for_OSLO[read_CSV_stats_per_day_for_OSLO['name'].isin(top4_visited_stations['name'])]

    sns.relplot(x='day', y='avg_queue_length', data=dataframes_for_oslo_west_stations, hue='name', kind='line')
    plt.suptitle("West Oslo")

    sns.relplot(x='day', y='avg_total_EV_in_station', data=dataframes_for_oslo_west_stations, hue='name', kind='line')
    plt.suptitle("West Oslo")

    sns.relplot(x='day', y='total_rejected_EVs', data=dataframes_for_oslo_west_stations, hue='name', kind='line')
    plt.suptitle("West Oslo")

    sns.relplot(x='day', y='total_dejected_EVs', data=dataframes_for_oslo_west_stations, hue='name', kind='line')
    plt.suptitle("West Oslo")


    plt.show()


def plot_results_about_oslo_south_east_stations():

    south_east = read_day_column[read_day_column['Region'] == 'south_east']

    top4_visited_stations = south_east.nlargest(15, 'total_visited_EVs')

    dataframes_for_oslo_south_east_stations = read_CSV_stats_per_day_for_OSLO[read_CSV_stats_per_day_for_OSLO['name'].isin(top4_visited_stations['name'])]

    sns.relplot(x='day', y='avg_queue_length', data=dataframes_for_oslo_south_east_stations, hue='name', kind='line')
    plt.suptitle("South-east")

    sns.relplot(x='day', y='avg_total_EV_in_station', data=dataframes_for_oslo_south_east_stations, hue='name', kind='line')
    plt.suptitle("South-east")

    sns.relplot(x='day', y='total_rejected_EVs', data=dataframes_for_oslo_south_east_stations, hue='name', kind='line')
    plt.suptitle("South-east")

    sns.relplot(x='day', y='total_dejected_EVs', data=dataframes_for_oslo_south_east_stations, hue='name', kind='line')
    plt.suptitle("South-east")

    plt.show()




#plot_results_about_oslo_north_east_stations()

#plot_results_about_oslo_north_west_stations()

#plot_results_about_oslo_north_stations()

#plot_results_about_oslo_west_stations()

#plot_results_about_oslo_centrum_middle_stations()

#plot_results_about_oslo_south_east_stations()




