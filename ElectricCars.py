import random
from termcolor import colored


NissanLeaf24kWh = None
NissanLeaf30kWh = None
BMWi3_22kWh = None
BMWi3_33kWh = None
Tesla_S90D = None
Tesla_X90D = None
Volkswagen_Egolf = None
KIAsoul = None
Volkswagen_E_up = None
Renault_ZOE = None
MercedesBenz_B250E = None
Hyundai_IONIC = None
all_EVs_collected = None

random_collection_of_EVs = None


class ElectricVehicles(object):
    charging_time_minutes_type1 = None
    charging_time_minutes_type2 = None
    charging_time_minutes_type3 = None


    time_used_at_type1 = None
    time_used_at_type2 = None
    time_used_at_type3 = None

    def __init__(self, model, charging_time_minutes_type1, charging_time_minutes_type2, charging_time_minutes_type3,patience_time_in_the_queue):

        self.model = model

        self.first_station_visited = []
        self.stations_visited = []

        self.time_used_at_type1 = 0
        self.time_used_at_type2 = 0
        self.time_used_at_type3 = 0

        if self.charging_time_minutes_type1:
            pass
        else:
            self.charging_time_minutes_type1 = random.choice(charging_time_minutes_type1)

        if self.charging_time_minutes_type2:
            pass
        else:
            self.charging_time_minutes_type2 = random.choice(charging_time_minutes_type2)

        if self.charging_time_minutes_type3:
            pass
        else:
            self.charging_time_minutes_type3 = random.choice(charging_time_minutes_type3)
        self.patience_time_in_the_queue = patience_time_in_the_queue

        "EV`s that has under 50% SoC when arriving to work stations:30 min patience time --> NissanLeaf 24kwh, BMWi3 22kwh,VW-Up(NOT GOLF)"
        "EV`s that has between 50 and 60% SoC when arriving to work: 15 min patience time--> NissanLeaf 30kwh, BMWi3 33kwh, Mercedes Benz B250e"
        "EV`s that has between 60 and 70% SoC when arriving to work: 10 min patience time--> Hyundai IONIC, KIA SOUL"
        "EV`s that has over 70% Soc(!) when arriving to work       : 5 min patience time --> Tesla S, Tesla X, VW-eGolf, Renault Z.O.E"

        self.time_in_queue = 0
        self.zero_patience_car_wants_to_leave = False
        self.is_fully_charged = False
        self.attempts = 2


    def __str__(self):
        return f"This is a {self.model}"


    def charge(self, type1speed):
        self.time_used_at_type1 += 1
        self.charging_time_minutes_type1 = max(0, self.charging_time_minutes_type1 - type1speed)
        if self.charging_time_minutes_type1 <= 0:
            self.is_fully_charged = True
            #print(colored("I have finished charging at [TYPE 1] - {}".format(self.model) + " - I used {} ".format(self.time_used_at_type1) + " minutes at this charger", 'red'))

    def charge2(self, type2speed):
        self.time_used_at_type2 += 1
        self.charging_time_minutes_type2 = max(0, self.charging_time_minutes_type2 - type2speed)
        if self.charging_time_minutes_type2 <= 0:
            self.is_fully_charged = True
            #print(colored("I have finished charging at [TYPE 2] - {}".format(self.model) + " - I used {} ".format(self.time_used_at_type2) + " minutes at this charger", 'blue'))

    def charge3(self, type3speed):
        self.time_used_at_type3 += 1
        self.charging_time_minutes_type3 = (max(0, self.charging_time_minutes_type3 - type3speed))
        if self.charging_time_minutes_type3 <= 0:
            self.is_fully_charged = True
            #print(colored("I have finished charging at [TYPE 3] - {}".format(self.model) + " - I used {} ".format(self.time_used_at_type3) + " minutes at this charger", 'green'))

    def update_patience_in_queue(self):
        self.time_in_queue += 1
        if self.time_in_queue == self.patience_time_in_the_queue:
            self.zero_patience_car_wants_to_leave = True

            #print("Car {} --> ".format(self.model) + ": I have been waiting for {}".format(self.time_in_queue) + " minutes! I am leaving")
            #print("---------------------------------------------------------------------------------------------------------------------")

    def decrease_attempts(self):
        if self.attempts == 2:
            #print("Lets see if I find another station.. {} ".format(self.model))
            print()

        if self.attempts == 1:
            #print("Trying 1 more time! {} ".format(self.model))
            print()

        if self.attempts == 0:
            #print("I am dejected! {} ".format(self.model))
            return False
        else:
            self.attempts -= 1
            return True

    def reset_queue_settings(self):
        self.time_in_queue = 0
        self.zero_patience_car_wants_to_leave = False


def generate_new_EVs():
    create_lists_of_EVs()
    create_the_EVs()
    create_random_collection_of_EVs()


def create_lists_of_EVs():
    global NissanLeaf24kWh
    NissanLeaf24kWh = []
    global NissanLeaf30kWh
    NissanLeaf30kWh = []
    global BMWi3_22kWh
    BMWi3_22kWh = []
    global BMWi3_33kWh
    BMWi3_33kWh = []
    global Tesla_S90D
    Tesla_S90D = []
    global Tesla_X90D
    Tesla_X90D = []
    global Volkswagen_Egolf
    Volkswagen_Egolf = []
    global KIAsoul
    KIAsoul = []
    global Volkswagen_E_up
    Volkswagen_E_up = []
    global Renault_ZOE
    Renault_ZOE = []
    global MercedesBenz_B250E
    MercedesBenz_B250E = []
    global Hyundai_IONIC
    Hyundai_IONIC = []
    global all_EVs_collected
    all_EVs_collected = []

    global random_collection_of_EVs
    random_collection_of_EVs = []


def create_the_EVs():
    for i in range(0, 7308):
        NissanLeaf24kWh.append(ElectricVehicles("Nissan leaf 24kWh pack", [211, 216, 269], [121, 126, 129],[round(9.453125), round(9.84375), round(13.984375)], 30))
        all_EVs_collected.append(ElectricVehicles("Nissan leaf 24kWh pack", [211, 216, 269], [121, 126, 129],[round(9.453125), round(9.84375), round(13.984375)], 30))

    for i in range(0, 7307):
        NissanLeaf30kWh.append(ElectricVehicles("Nissan leaf 30kWh pack", [180, 193, 247], [90, 103, 157],[round(7.03125), round(8.046875), round(12.265625)], 15))
        all_EVs_collected.append(ElectricVehicles("Nissan leaf 30kWh pack", [180, 193, 247], [90, 103, 157],[round(7.03125), round(8.046875), round(12.265625)], 15))

    for i in range(0, 2979):
        BMWi3_22kWh.append(ElectricVehicles("BMW i3 22kWh model", [245, 258, 316], [155, 168, 226],[round(12.109375), round(13.125), round(17.65625)], 30))
        all_EVs_collected.append(ElectricVehicles("BMW i3 22kWh model", [245, 258, 316], [155, 168, 226],[round(12.109375), round(13.125), round(17.65625)], 30))

    for i in range(0, 2979):
        BMWi3_33kWh.append(ElectricVehicles("BMW i3 33kWh model", [190, 203, 263], [100, 113, 173],[round(7.8125), round(8.828125), round(13.515625)], 15))
        all_EVs_collected.append(ElectricVehicles("BMW i3 33kWh model", [190, 203, 263], [100, 113, 173],[round(7.8125), round(8.828125), round(13.515625)], 15))

    for i in range(0, 6383):
        Tesla_S90D.append(ElectricVehicles("Tesla S 90 (D) model", [231, 251, 292], [141, 161, 202],[round(11.015625), round(12.578125), round(15.78125)], 5))
        all_EVs_collected.append(ElectricVehicles("Tesla S 90 (D) model", [231, 251, 292], [141, 161, 202],[round(11.015625), round(12.578125), round(15.78125)], 5))

    for i in range(0, 2858):
        Tesla_X90D.append(ElectricVehicles("Tesla X 90 D model", [191, 211, 251], [101, 121, 161],[round(7.890625), round(9.453125), round(12.578125)], 5))
        all_EVs_collected.append(ElectricVehicles("Tesla X 90 D model", [191, 211, 251], [101, 121, 161],[round(7.890625), round(9.453125), round(12.578125)], 5))

    for i in range(0, 9645):
        Volkswagen_Egolf.append(ElectricVehicles("Volkswagen E Golf", [148, 153, 182], [58, 63, 92],[round(4.53125), round(4.921875), round(7.1875)], 5))
        all_EVs_collected.append(ElectricVehicles("Volkswagen E Golf", [148, 153, 182], [58, 63, 92],[round(4.53125), round(4.921875), round(7.1875)], 5))

    for i in range(0, 4708):
        KIAsoul.append(ElectricVehicles("KIA soul", [166, 177, 215], [76, 87, 125],[round(5.9375), round(6.796875), round(9.765625)], 10))
        all_EVs_collected.append(ElectricVehicles("KIA soul", [166, 177, 215], [76, 87, 125],[round(5.9375), round(6.796875), round(9.765625)], 10))

    for i in range(0, 3195):
        Volkswagen_E_up.append(ElectricVehicles("Volkswagen E up(NOT GOLF!)", [245, 258, 316], [155, 168, 226],[round(12.109375), round(13.125), round(17.65625)], 30))
        all_EVs_collected.append(ElectricVehicles("Volkswagen E up(NOT GOLF!)", [245, 258, 316], [155, 168, 226],[round(12.109375), round(13.125), round(17.65625)], 30))

    for i in range(0, 2909):
        Renault_ZOE.append(ElectricVehicles("Renault Z.O.E", [162, 170, 195], [72, 80, 105],[round(5.625), round(6.25), round(8.203125)], 5))
        all_EVs_collected.append(ElectricVehicles("Renault Z.O.E", [162, 170, 195], [72, 80, 105],[round(5.625), round(6.25), round(8.203125)], 5))

    for i in range(0, 2139):
        MercedesBenz_B250E.append(ElectricVehicles("Mercedes Benz B250E", [186, 191, 248], [96, 101, 158],[round(7.5), round(7.890625), round(12.34375)], 15))
        all_EVs_collected.append(ElectricVehicles("Mercedes Benz B250E", [186, 191, 248], [96, 101, 158],[round(7.5), round(7.890625), round(12.34375)], 15))

    for i in range(0, 1424):
        Hyundai_IONIC.append(ElectricVehicles("Hyundai IONIC", [191, 140, 180], [101, 50, 90],[round(7.890625), round(3.90625), round(7.03125)], 10))
        all_EVs_collected.append(ElectricVehicles("Hyundai IONIC", [191, 140, 180], [101, 50, 90],[round(7.890625), round(3.90625), round(7.03125)], 10))



def create_random_collection_of_EVs():
    random_collection_of_EVs.extend(random.sample(all_EVs_collected, 53834))
    all_EVs_collected.clear()


def wipe_out_all_EVs():
    NissanLeaf24kWh.clear()
    NissanLeaf30kWh.clear()
    BMWi3_22kWh.clear()
    BMWi3_33kWh.clear()
    Tesla_S90D.clear()
    Tesla_X90D.clear()
    Volkswagen_Egolf.clear()
    KIAsoul.clear()
    Volkswagen_E_up.clear()
    Renault_ZOE.clear()
    MercedesBenz_B250E.clear()
    Hyundai_IONIC.clear()
    all_EVs_collected.clear()
    random_collection_of_EVs.clear()

