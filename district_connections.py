from ChargerStation import ChargingStation
from ChargerStation import district_names

from import_data_from_csv_into_simulation import graph
from collections import OrderedDict
import sys



chargingstations = {}
for i in range(len(district_names)):
    chargingstations[district_names[i]] = ChargingStation(district_names[i])

def create_neighbors_using_normal_graph():
    for node in graph:
        each_cs_is_a_node = chargingstations[node]
        for n in graph[node]:
            chargingstation_neighbors = chargingstations[n]
            each_cs_is_a_node.add_neighbor(chargingstation_neighbors)


create_neighbors_using_normal_graph()

collection_of_all_chargingstations_in_working_districts = []
collection_of_all_chargingstations_in_working_districts.append(chargingstations["Lysaker"])
collection_of_all_chargingstations_in_working_districts.append(chargingstations["Fornebu"])
collection_of_all_chargingstations_in_working_districts.append(chargingstations["Skøyen"])
collection_of_all_chargingstations_in_working_districts.append(chargingstations["Økern"])
collection_of_all_chargingstations_in_working_districts.append(chargingstations["Blindern & UiO"])
collection_of_all_chargingstations_in_working_districts.append(chargingstations["Ullevål-Stadion"])
collection_of_all_chargingstations_in_working_districts.append(chargingstations["Alnabru"])
collection_of_all_chargingstations_in_working_districts.append(chargingstations["Oslo City"])

collection_of_all_chargingstations_in_main_districts = []
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Lilleaker & CC-vest"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Ullern"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Vinderen"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Smestad"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Frogner"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Majorstuen"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Pilestredet"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["St.hanshaugen"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Nydalen"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Bjølsen"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Sagene"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Torshov"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Grunerløkka & Vulkan"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Carl-berner"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Grønland"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Tøyen & Kampen"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Helsfyr"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Ensjø"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Vallehovin & Ulven"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Alna"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Furuset & Ellingsrud"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations['Økern'])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Risløkka & Vollebekk"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Veitvet & Linderud"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Smedstua"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Old Oslo"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Ekeberg & Nordstrand"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Holmlia"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Søndre-Nordstrand"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Bryn & Manglerud"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Østensjø"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Lambertseter"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Mortensrud"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Sinsen"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Bjerke & Brobekk"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Kalbakken"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Grorud"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Stovner"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Storo"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Grefsen"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Kjelsås"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Blindern & UiO"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Marienlyst"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Fagerborg & Valle"])

collection_of_all_chargingstations_in_main_districts.append(chargingstations["Klemetsrud & E6-south"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Tangerud area & interchange 163/E6"])
collection_of_all_chargingstations_in_main_districts.append(chargingstations["Karihaugen area & interchange E6/163/159"])

collection_of_all_chargingstations_in_hotspot_districts = []
collection_of_all_chargingstations_in_hotspot_districts.append(chargingstations["Oslo City"])
collection_of_all_chargingstations_in_hotspot_districts.append(chargingstations["Kjelsås"])
collection_of_all_chargingstations_in_hotspot_districts.append(chargingstations["Vika-National theatre & Akershus fortress"])
collection_of_all_chargingstations_in_hotspot_districts.append(chargingstations["Grunerløkka & Vulkan"])
collection_of_all_chargingstations_in_hotspot_districts.append(chargingstations["Frogner"])

"""-------------------------------------Divide station by regions-------------------------------------------------"""
total_amount_of_chargers_in_north = 0
all_stations_in_north = []
all_stations_in_north.append(chargingstations["Kjelsås"])
all_stations_in_north.append(chargingstations["Grefsen"])
all_stations_in_north.append(chargingstations["Sinsen"])
all_stations_in_north.append(chargingstations["Nydalen"])
all_stations_in_north.append(chargingstations["Bjølsen"])
all_stations_in_north.append(chargingstations["Sagene"])
all_stations_in_north.append(chargingstations["Storo"])

total_amount_of_chargers_in_north_east = 0
all_stations_in_north_east = []
all_stations_in_north_east.append(chargingstations["Tangerud area & interchange 163/E6"])
all_stations_in_north_east.append(chargingstations["Karihaugen area & interchange E6/163/159"])
all_stations_in_north_east.append(chargingstations["Bjerke & Brobekk"])
all_stations_in_north_east.append(chargingstations["Kalbakken"])
all_stations_in_north_east.append(chargingstations["Grorud"])
all_stations_in_north_east.append(chargingstations["Stovner"])
all_stations_in_north_east.append(chargingstations['Økern'])
all_stations_in_north_east.append(chargingstations["Risløkka & Vollebekk"])
all_stations_in_north_east.append(chargingstations["Veitvet & Linderud"])
all_stations_in_north_east.append(chargingstations["Smedstua"])
all_stations_in_north_east.append(chargingstations["Alnabru"])
all_stations_in_north_east.append(chargingstations["Alna"])
all_stations_in_north_east.append(chargingstations["Furuset & Ellingsrud"])


total_amount_of_chargers_in_south_east = 0
all_stations_in_south_east = []
all_stations_in_south_east.append(chargingstations["Ekeberg & Nordstrand"])
all_stations_in_south_east.append(chargingstations["Holmlia"])
all_stations_in_south_east.append(chargingstations["Søndre-Nordstrand"])
all_stations_in_south_east.append(chargingstations["Vallehovin & Ulven"])
all_stations_in_south_east.append(chargingstations["Bryn & Manglerud"])
all_stations_in_south_east.append(chargingstations["Østensjø"])
all_stations_in_south_east.append(chargingstations["Lambertseter"])
all_stations_in_south_east.append(chargingstations["Mortensrud"])
all_stations_in_south_east.append(chargingstations["Helsfyr"])
all_stations_in_south_east.append(chargingstations["Klemetsrud & E6-south"])


total_amount_of_chargers_in_centrum_middle = 0
all_stations_in_centrum_middle = []
all_stations_in_centrum_middle.append(chargingstations["Grunerløkka & Vulkan"])
all_stations_in_centrum_middle.append(chargingstations["Carl-berner"])
all_stations_in_centrum_middle.append(chargingstations["Grønland"])
all_stations_in_centrum_middle.append(chargingstations["Tøyen & Kampen"])
all_stations_in_centrum_middle.append(chargingstations["Torshov"])
all_stations_in_centrum_middle.append(chargingstations["Pilestredet"])
all_stations_in_centrum_middle.append(chargingstations["St.hanshaugen"])
all_stations_in_centrum_middle.append(chargingstations["Ensjø"])
all_stations_in_centrum_middle.append(chargingstations["Old Oslo"])
all_stations_in_centrum_middle.append(chargingstations["Vika-National theatre & Akershus fortress"])
all_stations_in_centrum_middle.append(chargingstations["Fagerborg & Valle"])
all_stations_in_centrum_middle.append(chargingstations["Oslo City"])

total_amount_of_chargers_in_north_west = 0
all_stations_in_north_west = []
all_stations_in_north_west.append(chargingstations["Blindern & UiO"])
all_stations_in_north_west.append(chargingstations["Ullevål-Stadion"])
all_stations_in_north_west.append(chargingstations["Vinderen"])
all_stations_in_north_west.append(chargingstations["Smestad"])
all_stations_in_north_west.append(chargingstations["Marienlyst"])

total_amount_of_chargers_in_west = 0
all_stations_in_west = []
all_stations_in_west.append(chargingstations["Fornebu"])
all_stations_in_west.append(chargingstations["Skøyen"])
all_stations_in_west.append(chargingstations["Majorstuen"])
all_stations_in_west.append(chargingstations["Frogner"])
all_stations_in_west.append(chargingstations["Ullern"])
all_stations_in_west.append(chargingstations["Lysaker"])
all_stations_in_west.append(chargingstations["Lilleaker & CC-vest"])

"--------------------------------------Count chargers for each region------------------------------------"
for count_all_stations_in_west in all_stations_in_west:
    total_amount_of_chargers_in_west += len(count_all_stations_in_west.total_amount_of_chargers)

for count_all_stations_in_north_west in all_stations_in_north_west:
    total_amount_of_chargers_in_north_west += len(count_all_stations_in_north_west.total_amount_of_chargers)

for count_all_stations_in_north in all_stations_in_north:
    total_amount_of_chargers_in_north += len(count_all_stations_in_north.total_amount_of_chargers)

for count_all_stations_in_north_east in all_stations_in_north_east:
    total_amount_of_chargers_in_north_east += len(count_all_stations_in_north_east.total_amount_of_chargers)

for count_all_stations_in_south_east in all_stations_in_south_east:
    total_amount_of_chargers_in_south_east += len(count_all_stations_in_south_east.total_amount_of_chargers)

for count_all_stations_centrum_middle in all_stations_in_centrum_middle:
    total_amount_of_chargers_in_centrum_middle += len(count_all_stations_centrum_middle.total_amount_of_chargers)

count_all_chargers_together = (total_amount_of_chargers_in_west + total_amount_of_chargers_in_north_west + total_amount_of_chargers_in_north + total_amount_of_chargers_in_north_east + total_amount_of_chargers_in_south_east + total_amount_of_chargers_in_centrum_middle)



"--------------------------------------Use the list below to count all stations--------------------------"
list_of_all_chargingstations = []
list_of_all_chargingstations.extend(all_stations_in_north)
list_of_all_chargingstations.extend(all_stations_in_centrum_middle)
list_of_all_chargingstations.extend(all_stations_in_north_east)
list_of_all_chargingstations.extend(all_stations_in_south_east)
list_of_all_chargingstations.extend(all_stations_in_north_west)
list_of_all_chargingstations.extend(all_stations_in_west)


total_amount_of_charging_stations = 0
total_amount_of_charging_stations += len(list_of_all_chargingstations)

