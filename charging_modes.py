import logging
from termcolor import colored

class EVcharger:

    car = None
    charging_speed = 1
    finished_charging_at_type1 = 0
    finished_charging_at_type2 = 0
    finished_charging_at_type3 = 0

    finished_charging_at_extra_type2 = 0


class type1charger(EVcharger):
    def add_car_to_charger(self, car):
        self.car = car


        #print(colored("CAR ADDED to type 1 charger! {} ".format(self.car.model) + " -- Remaining charging time in minutes at TYPE 1 CHARGER: {} ".format(self.car.charging_time_minutes_type1),'red'))
        #print(colored("==============================================================================================================================================",'red'))

    def remove_car_from_charger(self):
        self.finished_charging_at_type1 += 1
        #print("EV removed from charger! {}".format(self.car.model))
        self.car = None


    def update_charger(self, StationSpeed):

        if self.car:
            
            charge_rate = float(self.charging_speed - StationSpeed)
     
            self.car.charge(charge_rate)


            if self.car.is_fully_charged:
                self.remove_car_from_charger()



class type2charger(EVcharger):
    def add_car_to_charger(self, car):
        self.car = car

        #print(colored("CAR ADDED to type 2 charger! {} ".format(self.car.model) + " -- charging time picked: {} ".format(self.car.charging_time_minutes_type2),'blue'))
        #print(colored("==============================================================================================================================================", 'blue'))


    def remove_car_from_charger(self):
        self.finished_charging_at_type2 += 1
        #print("EV removed from charger! {}".format(self.car.model))
        self.car = None


    def update_charger(self, StationSpeed):

        if self.car:

            charge_rate = float(self.charging_speed - StationSpeed)

            self.car.charge2(charge_rate)


            if self.car.is_fully_charged:
                self.remove_car_from_charger()




class type3charger(EVcharger):
    def add_car_to_charger(self, car):
        self.car = car

        #print(colored("CAR ADDED to type 3 charger! {} ".format(self.car.model) + " -- charging time picked: {} ".format(self.car.charging_time_minutes_type3),'green'))
        #print(colored("==============================================================================================================================================", 'green'))

    def remove_car_from_charger(self):
        self.finished_charging_at_type3 += 1
        #print("EV removed from charger! {}".format(self.car.model))
        self.car = None


    def update_charger(self, StationSpeed):
        if self.car:

            charge_rate = float(self.charging_speed - StationSpeed)

            self.car.charge3(charge_rate)


            if self.car.is_fully_charged:
                self.remove_car_from_charger()







