import csv

with open("chargers_in_oslo.csv", encoding='utf-8') as f:
    reader = csv.reader(f)

    regions = next(reader)
    district_names = next(reader)
    charge_points_schuko = [int(_) for _ in next(reader)]
    charge_points_type2 = [int(_) for _ in next(reader)]
    charge_points_superCharger = [int(_) for _ in next(reader)]

    Type1PLUSType2 = [int(_) for _ in next(reader)]  # this is not used

    charge_points_schuko_original = [int(_) for _ in next(reader)]   # OBS: those are real numbers
    charge_points_type2_original = [int(_) for _ in next(reader)]     # OBS: those are real numbers



districts = dict(zip(regions, zip(district_names,charge_points_schuko, charge_points_type2, charge_points_superCharger)))



if_i_want_to_count_all_original_chargers = dict(zip(district_names, zip(charge_points_schuko_original, charge_points_type2_original, charge_points_superCharger, Type1PLUSType2)))
count_all_type1_chargers = sum(charge_points_schuko_original)
count_all_type2_chargers = sum(charge_points_type2_original)
count_all_Type1_plus_type2 = sum(Type1PLUSType2)
count_all_super_chargers = sum(charge_points_superCharger)






"""=====================Underneath we connect the stations with each other in a graph================"""

graph = {'Smestad': ['Vinderen', 'Ullern', 'Majorstuen'],

         'Kjelsås': ['Grefsen', 'Storo'],

         'Ullevål-Stadion': ['Vinderen', 'Blindern & UiO', 'Nydalen', 'Storo'],

         'Vinderen': ['Ullevål-Stadion', 'Smestad', 'Blindern & UiO'],

         'Nydalen': ['Storo', 'Ullevål-Stadion', 'Bjølsen'],

         'Grunerløkka & Vulkan': ['Grønland', 'Oslo City', 'Torshov', 'Tøyen & Kampen', 'Carl-berner'],

         'Oslo City': ['Frogner', 'Grunerløkka & Vulkan', 'Grønland', 'Old Oslo', 'Vika-National theatre & Akershus fortress'],

         'Grønland': ['Oslo City', 'Grunerløkka & Vulkan', 'Tøyen & Kampen', 'Old Oslo'],

         'Vika-National theatre & Akershus fortress': ['Frogner', 'Oslo City', 'Pilestredet'],

         'Frogner': ['Skøyen', 'Oslo City', 'Vika-National theatre & Akershus fortress', 'Majorstuen'],

         'Majorstuen': ['St.hanshaugen', 'Pilestredet', 'Skøyen', 'Marienlyst', 'Frogner','Smestad', 'Fagerborg & Valle'],

         'Marienlyst': ['Majorstuen', 'Blindern & UiO', 'Fagerborg & Valle'],

         'Torshov': ['Storo', 'Sagene', 'Fagerborg & Valle', 'St.hanshaugen', 'Grunerløkka & Vulkan', 'Carl-berner','Sinsen'],

         'Storo': ['Grefsen', 'Kjelsås', 'Sinsen', 'Sagene', 'Torshov','Ullevål-Stadion','Nydalen'],

         'Østensjø': ['Bryn & Manglerud'],

         'Ullern': ['Lilleaker & CC-vest', 'Smestad', 'Lysaker'],

         'Fagerborg & Valle': ['Sagene', 'Torshov', 'Majorstuen','Marienlyst'],

         'Risløkka & Vollebekk': ['Økern', 'Veitvet & Linderud'],

         'Kalbakken': ['Bjerke & Brobekk', 'Grorud'],

         'Veitvet & Linderud': ['Risløkka & Vollebekk', 'Alnabru', 'Smedstua'],

         'Bryn & Manglerud': ['Østensjø', 'Helsfyr', 'Old Oslo', 'Lambertseter', 'Vallehovin & Ulven'],

         'Old Oslo':['Oslo City', 'Helsfyr', 'Tøyen & Kampen', 'Grønland', 'Bryn & Manglerud', 'Ekeberg & Nordstrand','Ensjø'],

         'Bjerke & Brobekk': ['Kalbakken', 'Sinsen', 'Økern'],

         'Alna': ['Furuset & Ellingsrud', 'Alnabru', 'Vallehovin & Ulven'],

         'Furuset & Ellingsrud': ['Karihaugen area & interchange E6/163/159', 'Alna'],

         'Grefsen': ['Storo', 'Kjelsås'],

         'Sagene': ['Torshov', 'Bjølsen', 'Fagerborg & Valle', 'Storo'],

         'St.hanshaugen': ['Majorstuen', 'Majorstuen', 'Pilestredet'],

         'Pilestredet': ['Majorstuen', 'St.hanshaugen', 'Vika-National theatre & Akershus fortress'],

         'Grorud': ['Stovner', 'Kalbakken'],

         'Stovner': ['Grorud', 'Smedstua'],

         'Ekeberg & Nordstrand': ['Old Oslo', 'Lambertseter', 'Holmlia'],

         'Søndre-Nordstrand': ['Holmlia', 'Klemetsrud & E6-south'],

         'Lambertseter': ['Bryn & Manglerud', 'Mortensrud', 'Ekeberg & Nordstrand'],

         'Mortensrud': ['Lambertseter', 'Klemetsrud & E6-south'],

         'Holmlia': ['Ekeberg & Nordstrand', 'Søndre-Nordstrand'],

         'Fornebu': ['Lysaker'],

         'Blindern & UiO': ['Marienlyst', 'Ullevål-Stadion', 'Vinderen'],

         'Helsfyr': ['Vallehovin & Ulven', 'Old Oslo', 'Bryn & Manglerud', 'Ensjø'],

         'Ensjø': ['Helsfyr', 'Tøyen & Kampen','Vallehovin & Ulven', 'Old Oslo'],

         'Alnabru': ['Alna', 'Veitvet & Linderud'],

         'Økern': ['Carl-berner', 'Risløkka & Vollebekk', 'Vallehovin & Ulven', 'Bjerke & Brobekk','Vallehovin & Ulven'],

         'Skøyen': ['Frogner', 'Lysaker', 'Majorstuen', 'Lilleaker & CC-vest'],

         'Karihaugen area & interchange E6/163/159': ['Tangerud area & interchange 163/E6', 'Furuset & Ellingsrud'],

         'Tangerud area & interchange 163/E6': ['Smedstua', 'Karihaugen area & interchange E6/163/159'],

         'Klemetsrud & E6-south': ['Søndre-Nordstrand', 'Mortensrud'],

         'Lysaker': ['Skøyen', 'Fornebu', 'Ullern', 'Lilleaker & CC-vest'],

         'Vallehovin & Ulven': ['Alna', 'Helsfyr', 'Økern', 'Ensjø','Bryn & Manglerud'],

         'Lilleaker & CC-vest': ['Lysaker', 'Skøyen', 'Ullern'],

         'Carl-berner': ['Sinsen', 'Torshov', 'Grunerløkka & Vulkan', 'Tøyen & Kampen', 'Økern'],

         'Tøyen & Kampen': ['Grønland', 'Carl-berner', 'Grunerløkka & Vulkan', 'Old Oslo', 'Ensjø'],

         'Bjølsen': ['Nydalen', 'Sagene'],

         'Sinsen': ['Storo', 'Carl-berner', 'Torshov','Bjerke & Brobekk','Økern'],

         'Smedstua': ['Stovner', 'Tangerud area & interchange 163/E6', 'Veitvet & Linderud']}

