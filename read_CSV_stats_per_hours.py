import pandas as pd
import random
import glob
import os
import matplotlib.pyplot as plt
import seaborn as sns
from termcolor import colored, cprint
import sys

random_day = random.randint(0, 150)




def plot_results_about_north_stations():
    read_the_CSV_file = pd.read_csv('results_per_hour/ALL_stations_together.csv', sep=";", encoding="ISO-8859-1")

    give_me_only_north_region = read_the_CSV_file[read_the_CSV_file['Region'] == 'north']

    give_me_day_three_only = give_me_only_north_region[give_me_only_north_region['day'] == random_day]


    sns.factorplot(x='hour', y='EVs_in_queue_per_hour', data=give_me_day_three_only, hue='name')
    plt.suptitle("North")

    sns.factorplot(x='hour', y='EVs_charging_totally', data=give_me_day_three_only, hue='name')
    plt.suptitle("North")

    plt.show()




def plot_results_about_west_stations():
    read_the_CSV_file = pd.read_csv('results_per_hour/ALL_stations_together.csv', sep=";", encoding="ISO-8859-1")

    give_me_only_north_region = read_the_CSV_file[read_the_CSV_file['Region'] == 'west']

    give_me_day_three_only = give_me_only_north_region[give_me_only_north_region['day'] == random_day]


    sns.factorplot(x='hour', y='EVs_in_queue_per_hour', data=give_me_day_three_only, hue='name')
    plt.suptitle("North")

    sns.factorplot(x='hour', y='EVs_charging_totally', data=give_me_day_three_only, hue='name')
    plt.suptitle("North")

    plt.show()


def plot_results_about_north_west_stations():
    read_the_CSV_file = pd.read_csv('results_per_hour/ALL_stations_together.csv', sep=";", encoding="ISO-8859-1")

    give_me_only_north_region = read_the_CSV_file[read_the_CSV_file['Region'] == 'north_west']

    give_me_day_three_only = give_me_only_north_region[give_me_only_north_region['day'] == random_day]


    sns.factorplot(x='hour', y='EVs_in_queue_per_hour', data=give_me_day_three_only, hue='name')
    plt.suptitle("North")

    sns.factorplot(x='hour', y='EVs_charging_totally', data=give_me_day_three_only, hue='name')
    plt.suptitle("North")

    plt.show()



#plot_results_about_west_stations()
plot_results_about_north_west_stations()
#plot_results_about_north_stations()
