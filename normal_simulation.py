import random
import logging
import Distributions

import sys
from termcolor import colored, cprint
from collections import namedtuple

import csv

import ElectricCars

from district_connections import collection_of_all_chargingstations_in_main_districts, \
    collection_of_all_chargingstations_in_hotspot_districts, collection_of_all_chargingstations_in_working_districts, \
    chargingstations
from district_connections import total_amount_of_charging_stations

logger = logging.getLogger("my_logger")
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("main_log.log")
fh.setLevel(logging.INFO)
sh = logging.StreamHandler()
sh.setLevel(logging.INFO)


class CustomFormatter(logging.Formatter):
    def __init__(self, msg):
        logging.Formatter.__init__(self, msg)

    def format(self, record):
        try:
            msg = record.msg.split(':', 1)
            if len(msg) == 2:
                record.msg = '[%s]%s' % (msg[0], msg[1])
        except:
            pass
        return logging.Formatter.format(self, record)


formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(CustomFormatter('[%(levelname)s] %(message)s'))
sh.setFormatter(CustomFormatter('[%(levelname)s] %(message)s'))

logger.addHandler(sh)
logger.addHandler(fh)

"---------------------------------------Create CSV files for hours: General info --------------------------------------"
with open("results_per_hour/hotspot_districts_results_from_simulation.csv", 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['day', 'hour', 'name', 'EVs_charging_at_type1', 'EVs_charging_at_type2', 'EVs_charging_at_type3',
         'EVs_charging_totally', 'percentage_of_chargers_in_use', 'EVs_in_queue_per_hour',
         'total_EV_in_station_per_hour', 'visited_EVs', 'entered_EVs', 'finished_EVs_at_type1',
         'finished_EVs_at_type2', 'finished_EVs_at_type3', 'total_finished_charging', 'rejected_EVs', 'reneged_EVs',
         'dejected_EVs', 'Distribution', 'Region'])

with open("results_per_hour/work_districts_results_from_simulation.csv", 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['day', 'hour', 'name', 'EVs_charging_at_type1', 'EVs_charging_at_type2', 'EVs_charging_at_type3',
         'EVs_charging_totally', 'percentage_of_chargers_in_use', 'EVs_in_queue_per_hour',
         'total_EV_in_station_per_hour', 'visited_EVs', 'entered_EVs', 'finished_EVs_at_type1',
         'finished_EVs_at_type2', 'finished_EVs_at_type3', 'total_finished_charging', 'rejected_EVs', 'reneged_EVs',
         'dejected_EVs', 'Distribution', 'Region'])

with open("results_per_hour/main_districts_results_from_simulation.csv", 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['day', 'hour', 'name', 'EVs_charging_at_type1', 'EVs_charging_at_type2', 'EVs_charging_at_type3',
         'EVs_charging_totally', 'percentage_of_chargers_in_use', 'EVs_in_queue_per_hour',
         'total_EV_in_station_per_hour', 'visited_EVs', 'entered_EVs', 'finished_EVs_at_type1',
         'finished_EVs_at_type2', 'finished_EVs_at_type3', 'total_finished_charging', 'rejected_EVs', 'reneged_EVs',
         'dejected_EVs', 'Distribution', 'Region'])

with open("results_per_hour/ALL_stations_together.csv", 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['day', 'hour', 'name', 'EVs_charging_at_type1', 'EVs_charging_at_type2', 'EVs_charging_at_type3',
         'EVs_charging_totally', 'percentage_of_chargers_in_use', 'EVs_in_queue_per_hour',
         'total_EV_in_station_per_hour', 'visited_EVs', 'entered_EVs', 'finished_EVs_at_type1',
         'finished_EVs_at_type2', 'finished_EVs_at_type3', 'total_finished_charging', 'rejected_EVs', 'reneged_EVs',
         'dejected_EVs', 'Distribution', 'Region'])

"---------------------------------------Create CSV files for days : General info---------------------------------------"
with open("results_per_day/hotspot_districts_results_from_simulation.csv", 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=";", quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['day', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers', 'type2_extra_chargers',
         'cost_in_NOK', 'type3_chargers', 'total_amount_of_chargers', 'avg_queue_length', 'avg_total_EV_in_station',
         'total_visited_EVs', 'total_entered_EVs', 'total_rejected_EVs', 'total_reneged_EVs',
         'total_finished_EVs_charging', 'total_dejected_EVs', 'Distribution', 'Region'])

with open("results_per_day/work_districts_results_from_simulation.csv", 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=";", quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['day', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers', 'type2_extra_chargers',
         'cost_in_NOK', 'type3_chargers', 'total_amount_of_chargers', 'avg_queue_length', 'avg_total_EV_in_station',
         'total_visited_EVs', 'total_entered_EVs', 'total_rejected_EVs', 'total_reneged_EVs',
         'total_finished_EVs_charging', 'total_dejected_EVs', 'Distribution', 'Region'])

with open("results_per_day/main_districts_results_from_simulation.csv", 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=";", quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['day', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers', 'type2_extra_chargers',
         'cost_in_NOK', 'type3_chargers', 'total_amount_of_chargers', 'avg_queue_length', 'avg_total_EV_in_station',
         'total_visited_EVs', 'total_entered_EVs', 'total_rejected_EVs', 'total_reneged_EVs',
         'total_finished_EVs_charging', 'total_dejected_EVs', 'Distribution', 'Region'])

with open("results_per_day/ALL_stations_together.csv", "a", newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=";", quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['day', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers', 'type2_extra_chargers',
         'cost_in_NOK', 'type3_chargers', 'total_amount_of_chargers', 'avg_queue_length', 'avg_total_EV_in_station',
         'total_visited_EVs', 'total_entered_EVs', 'total_rejected_EVs', 'total_reneged_EVs',
         'total_finished_EVs_charging', 'total_dejected_EVs', 'Distribution', 'Region'])

"-------------------------------------Create CSV files for average statistics------------------------------------------"
with open("results_per_day/avg_stats.csv", "a", newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=";", quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['day', 'extra_type1_chargers', 'extra_type2_chargers', 'total_extra_amount_of_chargers', 'cost_in_NOK',
         'total_reneged_EVs', 'total_rejected_EVs', 'total_dejected_EVs', 'reduced_reneged_EVs',
         'reduced_rejected_EVs', 'reduced_dejected_EVs',
         'total_amount_of_chargers', 'avg_reneged_EVs', 'avg_rejected_EVs', 'avg_dejected_EVs'])

"-----------------Here we put info about what happened every hour-------------------------"
hotspot_districts_stats_per_hours = []
hotspot_daily_infos = []

work_districts_stats_per_hours = []
work_districts_daily_infos = []

main_districts_stats_per_hours = []
main_districts_daily_infos = []

"-------------------These tuples are used for the day collection!------"
hotspot_hour_info = namedtuple('hotspot_hour_info',
                               ['hour', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers',
                                'type2_extra_chargers', 'cost_in_NOK', 'super_chargers', 'total_amount_of_chargers',
                                'hotspot_EVs_in_queue_per_hour', 'hotspot_total_EV_in_station_per_hour',
                                'hotspot_visited_EVs_per_hour', 'hotspot_entered_EVs_per_hour',
                                'hotspot_rejected_EVs_per_hour', 'hotspot_reneged_EVs_per_hour',
                                'hotspot_finished_EVs_charging_per_hour', 'hotspot_dejected_EVs_per_hour',
                                'hotspot_region'])
hotspot_day_info = namedtuple('hotspot_day_info',
                              ['day', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers',
                               'type2_extra_chargers', 'cost_in_NOK', 'super_chargers', 'total_amount_of_chargers',
                               'hotspot_avg_queue_length', 'hotspot_avg_total_EV_in_station',
                               'hotspot_total_visited_EVs', 'hotspot_total_entered_EVs',
                               'hotspot_total_rejected_EVs', 'hotspot_total_reneged_EVs',
                               'hotspot_total_finished_EVs_charging', 'hotspot_total_dejected_EVs',
                               'hotspot_district', 'hotspot_region'])

work_district_hour_info = namedtuple('work_hour_info',
                                     ['hour', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers',
                                      'type2_extra_chargers', 'cost_in_NOK', 'super_chargers',
                                      'total_amount_of_chargers', 'work_districts_EVs_in_queue_per_hour',
                                      'work_districts_total_EV_in_station_per_hour',
                                      'work_districts_visited_EVs_per_hour', 'work_districts_entered_EVs_per_hour',
                                      'work_districts_rejected_EVs_per_hour', 'work_districts_reneged_EVs_per_hour',
                                      'work_districts_finished_EVs_charging_per_hour',
                                      'work_districts_dejected_EVs_per_hour', 'work_district_region'])
work_district_day_info = namedtuple('work_day_info',
                                    ['day', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers',
                                     'type2_extra_chargers', 'cost_in_NOK', 'super_chargers',
                                     'total_amount_of_chargers', 'work_districts_avg_queue_length',
                                     'work_districts_avg_total_EV_in_station', 'work_districts_total_visited_EVs',
                                     'work_districts_total_entered_EVs', 'work_districts_total_rejected_EVs',
                                     'work_districts_total_reneged_EVs', 'work_districts_total_finished_EVs_charging',
                                     'work_districts_total_dejected_EVs', 'work_district', 'work_district_region'])

main_district_hour_info = namedtuple('main_hour_info',
                                     ['hour', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers',
                                      'type2_extra_chargers', 'cost_in_NOK', 'super_chargers',
                                      'total_amount_of_chargers', 'main_districts_EVs_in_queue_per_hour',
                                      'main_districts_total_EV_in_station_per_hour',
                                      'main_districts_visited_EVs_per_hour', 'main_districts_entered_EVs_per_hour',
                                      'main_districts_rejected_EVs_per_hour', 'main_districts_reneged_EVs_per_hour',
                                      'main_districts_finished_EVs_charging_per_hour',
                                      'main_districts_dejected_EVs_per_hour', 'main_district_region'])
main_district_day_info = namedtuple('main_day_info',
                                    ['day', 'name', 'type1_chargers', 'type1_extra_chargers', 'type2_chargers',
                                     'type2_extra_chargers', 'cost_in_NOK', 'super_chargers',
                                     'total_amount_of_chargers', 'main_districts_avg_queue_length',
                                     'main_districts_avg_total_EV_in_station', 'main_districts_total_visited_EVs',
                                     'main_districts_total_entered_EVs', 'main_districts_total_rejected_EVs',
                                     'main_districts_total_reneged_EVs', 'main_districts_total_finished_EVs_charging',
                                     'main_districts_total_dejected_EVs', 'main_district', 'main_district_region'])

"""---------------------------Here we create lists and variables for computations---------------------"""
price_of_adding_new_charger = 60000
original_amount_of_chargers_in_oslo = 2185

average_stats_per_day = []

list_of_days = []
list_of_total_new_type1_added = []
list_of_total_new_type2_added = []
list_of_total_new_chargers = []
list_of_total_amount_of_chargers = []
list_of_new_costs_of_implementing_new_chargers = []

list_of_avg_dejected_EVs = []
list_of_avg_reneged_EVs = []
list_of_avg_rejected_EVs = []

list_of_total_rejected_EVs = []
list_of_total_reneged_EVs = []
list_of_total_dejected_EVs = []

list_of_difference_in_reduction_of_reneged_EVs = []
list_of_difference_in_reduction_of_rejected_EVs = []
list_of_difference_in_reduction_of_dejected_EVs = []

"""---------------------------------------The main simulation loop-------------------------------------"""



# choose_budget_per_station = int(input("what should be budget for each station?"))
choose_amount_of_day_to_run_simulation = int(input("How many days do you want the simulation to be running?"))

try:
    day_iterator = 0
    for days in range(0, choose_amount_of_day_to_run_simulation):

        ElectricCars.generate_new_EVs()
        Distributions.generate_new_data()

        hotspot_collect_daily_stats = {}
        main_districts_collect_daily_stats = {}
        work_districts_collect_daily_stats = {}

        for hour in range(0, 25):

            hotspot_iterator = 0
            for hotspot in collection_of_all_chargingstations_in_hotspot_districts:

                if len(ElectricCars.random_collection_of_EVs) != 0:
                    logger.info(f"hour {hour}: EVs at this hour: {Distributions.EVs_per_hour_to_hotspot_districts[hotspot_iterator][hour]}:")
                    for EVs in range(0, Distributions.EVs_per_hour_to_hotspot_districts[hotspot_iterator][hour]):
                        random_pick_of_EVs = random.choice(ElectricCars.random_collection_of_EVs)
                        hotspot.add_car_from_distribution(random_pick_of_EVs)
                        ElectricCars.random_collection_of_EVs.remove(random_pick_of_EVs)

                        if len(ElectricCars.random_collection_of_EVs) == 0:
                            break

                hotspot_iterator += 1
                logger.info(f"Report: for Hotspot district: {hotspot}")
                logger.info(colored("LIVE STATUS:  \n{}".format(hotspot.print_status_of_station()), 'green'))

                hotspot_record = namedtuple("hotspot_hour_info", (
                    'days', 'hour', 'name', 'EVs_charging_at_type1', 'EVs_charging_at_type2',
                    'EVs_charging_at_type3',
                    'EVs_charging_totally', 'percentage_of_chargers_in_use', 'EVs_in_queue',
                    'total_EV_in_station_per_hour', 'visited_EVs', 'entered_EVs', 'finished_EVs_at_type1',
                    'finished_EVs_at_type2', 'finished_EVs_at_type3', 'total_finished_charging', 'rejected_EVs',
                    'reneged_EVs', 'dejected_EVs', 'hotspot_code', 'region'))
                hotspot_districts_stats_per_hours.append(hotspot_record(
                    days=days,
                    hour=hour,
                    name=hotspot.station_name,
                    EVs_charging_at_type1=hotspot.amount_of_EVs_charging_at_type1,
                    EVs_charging_at_type2=hotspot.amount_of_EVs_charging_at_type2,
                    EVs_charging_at_type3=hotspot.amount_of_EVs_charging_at_type3,
                    EVs_charging_totally=hotspot.total_chargers_in_use,
                    percentage_of_chargers_in_use=hotspot.percentage_of_chargers_in_use,
                    EVs_in_queue=len(hotspot.EVs_waiting_in_queue),
                    total_EV_in_station_per_hour=hotspot.total_EVs_in_station,
                    visited_EVs=hotspot.total_visited_EVs,
                    entered_EVs=hotspot.total_EVs_entered_into_station,
                    finished_EVs_at_type1=hotspot.total_amount_of_EVs_finished_charging_at_type1,
                    finished_EVs_at_type2=hotspot.total_amount_of_EVs_finished_charging_at_type2,
                    finished_EVs_at_type3=hotspot.total_amount_of_EVs_finished_charging_at_type3,
                    total_finished_charging=hotspot.total_finished_EVs_charging,
                    rejected_EVs=hotspot.rejected_EVs,
                    reneged_EVs=hotspot.reneged_EVs,
                    dejected_EVs=len(hotspot.dejected_EVs),
                    hotspot_code='hotspot_district',
                    region=hotspot.regions))

                hotspot_numbers_of_extra_type1_chargers_added = len(
                    hotspot.use_this_list_to_hold_current_amount_of_new_type1_chargers_added)
                hotspot_numbers_of_extra_type2_chargers_added = len(
                    hotspot.use_this_list_to_hold_current_amount_of_new_type2_chargers_added)
                hotspot_total_cost_of_implementing_new_chargers = hotspot.total_amount_of_cost_of_placing_new_chargers_at_this_station
                hotspot_count_all_chargers = len(hotspot.total_amount_of_chargers)

                hotspot_EVs_in_queue_per_hour = len(hotspot.EVs_waiting_in_queue)
                hotspot_total_EV_in_station_per_hour = hotspot.total_EVs_in_station
                hotspot_visited_EVs_per_hour = hotspot.total_visited_EVs
                hotspot_entered_EVs_per_hour = hotspot.total_EVs_entered_into_station
                hotspot_rejected_EVs_per_hour = hotspot.rejected_EVs
                hotspot_reneged_EVs_per_hour = hotspot.reneged_EVs
                hotspot_finished_EVs_charging_per_hour = hotspot.total_finished_EVs_charging
                hotspot_dejected_EVs_per_hour = len(hotspot.dejected_EVs)

                hotspot_collect_daily_stats.setdefault(hotspot.station_name, []).append(
                    hotspot_hour_info(hour, hotspot.station_name, hotspot.type_1_charger,
                                      hotspot_numbers_of_extra_type1_chargers_added, hotspot.type_2_charger,
                                      hotspot_numbers_of_extra_type2_chargers_added,
                                      hotspot_total_cost_of_implementing_new_chargers, hotspot.super_charger,
                                      hotspot_count_all_chargers,
                                      hotspot_EVs_in_queue_per_hour, hotspot_total_EV_in_station_per_hour,
                                      hotspot_visited_EVs_per_hour, hotspot_entered_EVs_per_hour,
                                      hotspot_rejected_EVs_per_hour, hotspot_reneged_EVs_per_hour,
                                      hotspot_finished_EVs_charging_per_hour, hotspot_dejected_EVs_per_hour,
                                      hotspot.regions))

                for minut in range(0, 60):

                    EVs_currently_charging_at_type1 = 0
                    EVs_currently_charging_at_type2 = 0
                    EVs_currently_charging_at_type3 = 0

                    for charger1 in hotspot.type_1_all_chargers:
                        if charger1.car:
                            EVs_currently_charging_at_type1 += 1
                    charging_rate_type1 = EVs_currently_charging_at_type1 * 0.0003

                    for charger2 in hotspot.type_2_all_chargers:
                        if charger2.car:
                            EVs_currently_charging_at_type2 += 1
                    charging_rate_type2 = EVs_currently_charging_at_type2 * 0.0003

                    for charger3 in hotspot.type_3_all_chargers:
                        if charger3.car:
                            EVs_currently_charging_at_type3 += 1
                    charging_rate_type3 = EVs_currently_charging_at_type3 * 0.0003
                    total_charging_rate = (charging_rate_type1 + charging_rate_type2 + charging_rate_type3)

                    for charger1 in hotspot.type_1_all_chargers:
                        charger1.update_charger(float(total_charging_rate))

                    for charger2 in hotspot.type_2_all_chargers:
                        charger2.update_charger(float(total_charging_rate))

                    for charger3 in hotspot.type_3_all_chargers:
                        charger3.update_charger(float(total_charging_rate))

                    hotspot.update_patience_and_status_for_EVs_in_queue()
                    hotspot.grab_car_from_waiting_queue_and_place_to_charger()

            work_district_iterator = 0
            for work in collection_of_all_chargingstations_in_working_districts:
                print(len(ElectricCars.random_collection_of_EVs))

                if len(ElectricCars.random_collection_of_EVs) != 0:
                    logger.info(f"hour {hour}: EVs at this hour: {Distributions.EVs_per_hour_to_work_districts[work_district_iterator][hour]}:")
                    for EVs in range(0, Distributions.EVs_per_hour_to_work_districts[work_district_iterator][hour]):
                        random_pick_of_EVs = random.choice(ElectricCars.random_collection_of_EVs)
                        work.add_car_from_distribution(random_pick_of_EVs)
                        ElectricCars.random_collection_of_EVs.remove(random_pick_of_EVs)

                        if len(ElectricCars.random_collection_of_EVs) == 0:
                            break

                work_district_iterator += 1
                logger.info(f"Report for Work district: {work}")
                logger.info(colored("LIVE STATUS:  \n{}".format(work.print_status_of_station()), 'red'))

                work_districts_record = namedtuple("work_district_hour_info", (
                    'days', 'hour', 'name', 'EVs_charging_at_type1', 'EVs_charging_at_type2',
                    'EVs_charging_at_type3',
                    'EVs_charging_totally', 'percentage_of_chargers_in_use', 'EVs_in_queue',
                    'total_EV_in_station_per_hour', 'visited_EVs', 'entered_EVs', 'finished_EVs_at_type1',
                    'finished_EVs_at_type2', 'finished_EVs_at_type3', 'total_finished_charging', 'rejected_EVs',
                    'reneged_EVs', 'dejected_EVs', 'work_districts_code', 'region'))
                work_districts_stats_per_hours.append(work_districts_record(
                    days=days,
                    hour=hour,
                    name=work.station_name,
                    EVs_charging_at_type1=work.amount_of_EVs_charging_at_type1,
                    EVs_charging_at_type2=work.amount_of_EVs_charging_at_type2,
                    EVs_charging_at_type3=work.amount_of_EVs_charging_at_type3,
                    EVs_charging_totally=work.total_chargers_in_use,
                    percentage_of_chargers_in_use=work.percentage_of_chargers_in_use,
                    EVs_in_queue=len(work.EVs_waiting_in_queue),
                    total_EV_in_station_per_hour=work.total_EVs_in_station,
                    visited_EVs=work.total_visited_EVs,
                    entered_EVs=work.total_EVs_entered_into_station,
                    finished_EVs_at_type1=work.total_amount_of_EVs_finished_charging_at_type1,
                    finished_EVs_at_type2=work.total_amount_of_EVs_finished_charging_at_type2,
                    finished_EVs_at_type3=work.total_amount_of_EVs_finished_charging_at_type3,
                    total_finished_charging=work.total_finished_EVs_charging,
                    rejected_EVs=work.rejected_EVs,
                    reneged_EVs=work.reneged_EVs,
                    dejected_EVs=len(work.dejected_EVs),
                    work_districts_code='work_district',
                    region=work.regions))

                work_districts_numbers_of_extra_type1_chargers_added = len(
                    work.use_this_list_to_hold_current_amount_of_new_type1_chargers_added)
                work_districts_numbers_of_extra_type2_chargers_added = len(
                    work.use_this_list_to_hold_current_amount_of_new_type2_chargers_added)
                work_districts_total_cost_of_implementing_new_chargers = work.total_amount_of_cost_of_placing_new_chargers_at_this_station
                work_districts_count_all_chargers = len(work.total_amount_of_chargers)

                work_districts_EVs_in_queue_per_hour = len(work.EVs_waiting_in_queue)
                work_districts_total_EV_in_station_per_hour = work.total_EVs_in_station
                work_districts_visited_EVs_per_hour = work.total_visited_EVs
                work_districts_entered_EVs_per_hour = work.total_EVs_entered_into_station
                work_districts_rejected_EVs_per_hour = work.rejected_EVs
                work_districts_reneged_EVs_per_hour = work.reneged_EVs
                work_districts_finished_EVs_charging_per_hour = work.total_finished_EVs_charging
                work_districts_dejected_EVs_per_hour = len(work.dejected_EVs)

                work_districts_collect_daily_stats.setdefault(work.station_name, []).append(
                    work_district_hour_info(hour, work.station_name, work.type_1_charger,
                                            work_districts_numbers_of_extra_type1_chargers_added, work.type_2_charger,
                                            work_districts_numbers_of_extra_type2_chargers_added,
                                            work_districts_total_cost_of_implementing_new_chargers, work.super_charger,
                                            work_districts_count_all_chargers,
                                            work_districts_EVs_in_queue_per_hour,
                                            work_districts_total_EV_in_station_per_hour,
                                            work_districts_visited_EVs_per_hour, work_districts_entered_EVs_per_hour,
                                            work_districts_rejected_EVs_per_hour, work_districts_reneged_EVs_per_hour,
                                            work_districts_finished_EVs_charging_per_hour,
                                            work_districts_dejected_EVs_per_hour, work.regions))

                for minut in range(0, 60):

                    EVs_currently_charging_at_type1 = 0
                    EVs_currently_charging_at_type2 = 0
                    EVs_currently_charging_at_type3 = 0

                    for charger1 in work.type_1_all_chargers:
                        if charger1.car:
                            EVs_currently_charging_at_type1 += 1
                    charging_rate_type1 = EVs_currently_charging_at_type1 * 0.0003

                    for charger2 in work.type_2_all_chargers:
                        if charger2.car:
                            EVs_currently_charging_at_type2 += 1
                    charging_rate_type2 = EVs_currently_charging_at_type2 * 0.0003

                    for charger3 in work.type_3_all_chargers:
                        if charger3.car:
                            EVs_currently_charging_at_type3 += 1
                    charging_rate_type3 = EVs_currently_charging_at_type3 * 0.0003
                    total_charging_rate = (charging_rate_type1 + charging_rate_type2 + charging_rate_type3)

                    for charger1 in work.type_1_all_chargers:
                        charger1.update_charger(float(total_charging_rate))

                    for charger2 in work.type_2_all_chargers:
                        charger2.update_charger(float(total_charging_rate))

                    for charger3 in work.type_3_all_chargers:
                        charger3.update_charger(float(total_charging_rate))

                    work.update_patience_and_status_for_EVs_in_queue()
                    work.grab_car_from_waiting_queue_and_place_to_charger()

            main_district_iterator = 0
            for main in collection_of_all_chargingstations_in_main_districts:
                if len(ElectricCars.random_collection_of_EVs) != 0:

                    logger.info(f"hour {hour}: EVs at this hour: {Distributions.EVs_per_hour_to_main_districts[main_district_iterator][hour]}")
                    for EVs in range(0, Distributions.EVs_per_hour_to_main_districts[main_district_iterator][hour]):
                        random_pick_of_EVs = random.choice(ElectricCars.random_collection_of_EVs)
                        main.add_car_from_distribution(random_pick_of_EVs)
                        ElectricCars.random_collection_of_EVs.remove(random_pick_of_EVs)

                        if len(ElectricCars.random_collection_of_EVs) == 0:
                            break

                    main_district_iterator += 1
                    logger.info(f"Report for Main district: {main}")
                    logger.info(colored("LIVE STATUS: \n{}".format(main.print_status_of_station()), 'blue'))

                    main_districts_record = namedtuple("main_districts_hour_info", (
                        'days', 'hour', 'name', 'EVs_charging_at_type1', 'EVs_charging_at_type2',
                        'EVs_charging_at_type3', 'EVs_charging_totally', 'percentage_of_chargers_in_use',
                        'EVs_in_queue',
                        'total_EV_in_station_per_hour', 'visited_EVs', 'entered_EVs', 'finished_EVs_at_type1',
                        'finished_EVs_at_type2', 'finished_EVs_at_type3', 'total_finished_charging', 'rejected_EVs',
                        'reneged_EVs', 'dejected_EVs', 'main_districts_code', 'region'))
                    main_districts_stats_per_hours.append(main_districts_record(
                        days=days,
                        hour=hour,
                        name=main.station_name,
                        EVs_charging_at_type1=main.amount_of_EVs_charging_at_type1,
                        EVs_charging_at_type2=main.amount_of_EVs_charging_at_type2,
                        EVs_charging_at_type3=main.amount_of_EVs_charging_at_type3,
                        EVs_charging_totally=main.total_chargers_in_use,
                        percentage_of_chargers_in_use=main.percentage_of_chargers_in_use,
                        EVs_in_queue=len(main.EVs_waiting_in_queue),
                        total_EV_in_station_per_hour=main.total_EVs_in_station,
                        visited_EVs=main.total_visited_EVs,
                        entered_EVs=main.total_EVs_entered_into_station,
                        finished_EVs_at_type1=main.total_amount_of_EVs_finished_charging_at_type1,
                        finished_EVs_at_type2=main.total_amount_of_EVs_finished_charging_at_type2,
                        finished_EVs_at_type3=main.total_amount_of_EVs_finished_charging_at_type3,
                        total_finished_charging=main.total_finished_EVs_charging,
                        rejected_EVs=main.rejected_EVs,
                        reneged_EVs=main.reneged_EVs,
                        dejected_EVs=len(main.dejected_EVs),
                        main_districts_code='main_district',
                        region=main.regions))

                    main_districts_numbers_of_extra_type1_chargers_added = len(
                        main.use_this_list_to_hold_current_amount_of_new_type1_chargers_added)
                    main_districts_numbers_of_extra_type2_chargers_added = len(
                        main.use_this_list_to_hold_current_amount_of_new_type2_chargers_added)
                    main_districts_total_cost_of_implementing_new_chargers = main.total_amount_of_cost_of_placing_new_chargers_at_this_station
                    main_districts_count_all_chargers = len(main.total_amount_of_chargers)

                    main_districts_EVs_in_queue_per_hour = len(main.EVs_waiting_in_queue)
                    main_districts_total_EV_in_station_per_hour = main.total_EVs_in_station
                    main_districts_visited_EVs_per_hour = main.total_visited_EVs
                    main_districts_entered_EVs_per_hour = main.total_EVs_entered_into_station
                    main_districts_rejected_EVs_per_hour = main.rejected_EVs
                    main_districts_reneged_EVs_per_hour = main.reneged_EVs
                    main_districts_finished_EVs_charging_per_hour = main.total_finished_EVs_charging
                    main_districts_dejected_EVs_per_hour = len(main.dejected_EVs)

                    main_districts_collect_daily_stats.setdefault(main.station_name, []).append(
                        main_district_hour_info(hour, main.station_name, main.type_1_charger,
                                                main_districts_numbers_of_extra_type1_chargers_added,
                                                main.type_2_charger,
                                                main_districts_numbers_of_extra_type2_chargers_added,
                                                main_districts_total_cost_of_implementing_new_chargers,
                                                main.super_charger, main_districts_count_all_chargers,
                                                main_districts_EVs_in_queue_per_hour,
                                                main_districts_total_EV_in_station_per_hour,
                                                main_districts_visited_EVs_per_hour,
                                                main_districts_entered_EVs_per_hour,
                                                main_districts_rejected_EVs_per_hour,
                                                main_districts_reneged_EVs_per_hour,
                                                main_districts_finished_EVs_charging_per_hour,
                                                main_districts_dejected_EVs_per_hour, main.regions))

                    for minut in range(0, 60):

                        EVs_currently_charging_at_type1 = 0
                        EVs_currently_charging_at_type2 = 0
                        EVs_currently_charging_at_type3 = 0

                        for charger1 in main.type_1_all_chargers:
                            if charger1.car:
                                EVs_currently_charging_at_type1 += 1
                        charging_rate_type1 = EVs_currently_charging_at_type1 * 0.0003
                        for charger2 in main.type_2_all_chargers:
                            if charger2.car:
                                EVs_currently_charging_at_type2 += 1
                        charging_rate_type2 = EVs_currently_charging_at_type2 * 0.0003

                        for charger3 in main.type_3_all_chargers:
                            if charger3.car:
                                EVs_currently_charging_at_type3 += 1
                        charging_rate_type3 = EVs_currently_charging_at_type3 * 0.0003
                        total_charging_rate = (charging_rate_type1 + charging_rate_type2 + charging_rate_type3)
                        for charger1 in main.type_1_all_chargers:
                            charger1.update_charger(float(total_charging_rate))

                        for charger2 in main.type_2_all_chargers:
                            charger2.update_charger(float(total_charging_rate))

                        for charger3 in main.type_3_all_chargers:
                            charger3.update_charger(float(total_charging_rate))

                        main.update_patience_and_status_for_EVs_in_queue()
                        main.grab_car_from_waiting_queue_and_place_to_charger()

        for hotspot in collection_of_all_chargingstations_in_hotspot_districts:
            hotspot_count_all_chargers = len(hotspot.total_amount_of_chargers)

            hotspot_avg_queue_length = sum(hinfo.hotspot_EVs_in_queue_per_hour for hinfo in
                                           hotspot_collect_daily_stats[hotspot.station_name]) / 25
            hotspot_avg_total_EV_in_station = sum(hinfo.hotspot_total_EV_in_station_per_hour for hinfo in
                                                  hotspot_collect_daily_stats[hotspot.station_name]) / 25
            hotspot_total_visisted_EVs = max(
                hinfo.hotspot_visited_EVs_per_hour for hinfo in hotspot_collect_daily_stats[hotspot.station_name])
            hotspot_total_entered_EVs = max(
                hinfo.hotspot_entered_EVs_per_hour for hinfo in hotspot_collect_daily_stats[hotspot.station_name])
            hotspot_total_rejected_EVs = max(
                hinfo.hotspot_rejected_EVs_per_hour for hinfo in hotspot_collect_daily_stats[hotspot.station_name])
            hotspot_total_reneged_EVs = max(
                hinfo.hotspot_reneged_EVs_per_hour for hinfo in hotspot_collect_daily_stats[hotspot.station_name])
            hotspot_total_finished_EVs_charging = max(hinfo.hotspot_finished_EVs_charging_per_hour for hinfo in
                                                       hotspot_collect_daily_stats[hotspot.station_name])
            hotspot_total_dejected_EVs = max(
                hinfo.hotspot_dejected_EVs_per_hour for hinfo in hotspot_collect_daily_stats[hotspot.station_name])

            hotspot_daily_infos.append(hotspot_day_info(days, hotspot.station_name, hotspot.type_1_charger,
                                                        hotspot_numbers_of_extra_type1_chargers_added,
                                                        hotspot.type_2_charger,
                                                        hotspot_numbers_of_extra_type2_chargers_added,
                                                        hotspot.total_amount_of_cost_of_placing_new_chargers_at_this_station,
                                                        hotspot.super_charger, hotspot_count_all_chargers,
                                                        hotspot_avg_queue_length, hotspot_avg_total_EV_in_station,
                                                        hotspot_total_visisted_EVs, hotspot_total_entered_EVs,
                                                        hotspot_total_rejected_EVs, hotspot_total_reneged_EVs,
                                                        hotspot_total_finished_EVs_charging,
                                                        hotspot_total_dejected_EVs, 'hotspot_district',
                                                        hotspot.regions))

        for work in collection_of_all_chargingstations_in_working_districts:
            work_districts_count_all_chargers = len(work.total_amount_of_chargers)

            work_districts_avg_queue_length = sum(winfo.work_districts_EVs_in_queue_per_hour for winfo in
                                                  work_districts_collect_daily_stats[work.station_name]) / 25
            work_districts_avg_total_EV_in_station = sum(winfo.work_districts_total_EV_in_station_per_hour for winfo in
                                                         work_districts_collect_daily_stats[work.station_name]) / 25
            work_districts_total_visited_EVs = max(winfo.work_districts_visited_EVs_per_hour for winfo in
                                                    work_districts_collect_daily_stats[work.station_name])
            work_districts_total_entered_EVs = max(winfo.work_districts_entered_EVs_per_hour for winfo in
                                                    work_districts_collect_daily_stats[work.station_name])
            work_districts_total_rejected_EVs = max(winfo.work_districts_rejected_EVs_per_hour for winfo in
                                                     work_districts_collect_daily_stats[work.station_name])
            work_districts_total_reneged_EVs = max(winfo.work_districts_reneged_EVs_per_hour for winfo in
                                                    work_districts_collect_daily_stats[work.station_name])
            work_districts_total_finished_EVs_charging = max(
                winfo.work_districts_finished_EVs_charging_per_hour for winfo in
                work_districts_collect_daily_stats[work.station_name])
            work_districts_total_dejected_EVs = max(winfo.work_districts_dejected_EVs_per_hour for winfo in
                                                      work_districts_collect_daily_stats[work.station_name])

            work_districts_daily_infos.append(work_district_day_info(days, work.station_name, work.type_1_charger,
                                                                     work_districts_numbers_of_extra_type1_chargers_added,
                                                                     work.type_2_charger,
                                                                     work_districts_numbers_of_extra_type2_chargers_added,
                                                                     work.total_amount_of_cost_of_placing_new_chargers_at_this_station,
                                                                     work.super_charger,
                                                                     work_districts_count_all_chargers,
                                                                     work_districts_avg_queue_length,
                                                                     work_districts_avg_total_EV_in_station,
                                                                     work_districts_total_visited_EVs,
                                                                     work_districts_total_entered_EVs,
                                                                     work_districts_total_rejected_EVs,
                                                                     work_districts_total_reneged_EVs,
                                                                     work_districts_total_finished_EVs_charging,
                                                                     work_districts_total_dejected_EVs,
                                                                     'work_district', work.regions))

        for main in collection_of_all_chargingstations_in_main_districts:
            main_districts_count_all_chargers = len(main.total_amount_of_chargers)

            main_districts_avg_queue_length = sum(minfo.main_districts_EVs_in_queue_per_hour for minfo in
                                                  main_districts_collect_daily_stats[main.station_name]) / 25
            main_districts_avg_total_EV_in_station = sum(minfo.main_districts_total_EV_in_station_per_hour for minfo in
                                                         main_districts_collect_daily_stats[main.station_name]) / 25
            main_districts_total_visited_EVs = max(minfo.main_districts_visited_EVs_per_hour for minfo in
                                                    main_districts_collect_daily_stats[main.station_name])
            main_districts_total_entered_EVs = max(minfo.main_districts_entered_EVs_per_hour for minfo in
                                                    main_districts_collect_daily_stats[main.station_name])
            main_districts_total_rejected_EVs = max(minfo.main_districts_rejected_EVs_per_hour for minfo in
                                                     main_districts_collect_daily_stats[main.station_name])
            main_districts_total_reneged_EVs = max(minfo.main_districts_reneged_EVs_per_hour for minfo in
                                                    main_districts_collect_daily_stats[main.station_name])
            main_districts_total_finished_EVs_charging = max(
                minfo.main_districts_finished_EVs_charging_per_hour for minfo in
                main_districts_collect_daily_stats[main.station_name])
            main_districts_total_dejected_EVs = max(minfo.main_districts_dejected_EVs_per_hour for minfo in
                                                      main_districts_collect_daily_stats[main.station_name])

            main_districts_daily_infos.append(main_district_day_info(days, main.station_name, main.type_1_charger,
                                                                     main_districts_numbers_of_extra_type1_chargers_added,
                                                                     main.type_2_charger,
                                                                     main_districts_numbers_of_extra_type2_chargers_added,
                                                                     main.total_amount_of_cost_of_placing_new_chargers_at_this_station,
                                                                     main.super_charger,
                                                                     main_districts_count_all_chargers,
                                                                     main_districts_avg_queue_length,
                                                                     main_districts_avg_total_EV_in_station,
                                                                     main_districts_total_visited_EVs,
                                                                     main_districts_total_entered_EVs,
                                                                     main_districts_total_rejected_EVs,
                                                                     main_districts_total_reneged_EVs,
                                                                     main_districts_total_finished_EVs_charging,
                                                                     main_districts_total_dejected_EVs,
                                                                     'main_district', main.regions))

        print(f"Day {day_iterator}: finished!")
        print()
        list_of_days.append(day_iterator)
        day_iterator += 1

        """------------Here we calculate the numbers-------"""

        count_all_new_type1_chargers = 0
        for cs in chargingstations.values():
            count_all_new_type1_chargers += len(cs.use_this_list_to_hold_current_amount_of_new_type1_chargers_added)
        list_of_total_new_type1_added.append(count_all_new_type1_chargers)

        count_all_new_type2_chargers = 0
        for cs in chargingstations.values():
            count_all_new_type2_chargers += len(cs.use_this_list_to_hold_current_amount_of_new_type2_chargers_added)
        list_of_total_new_type2_added.append(count_all_new_type2_chargers)

        count_all_chargers_in_oslo = 0
        for cs in chargingstations.values():
            count_all_chargers_in_oslo += len(cs.total_amount_of_chargers)
        list_of_total_amount_of_chargers.append(count_all_chargers_in_oslo)

        new_total_amount_of_chargers = count_all_chargers_in_oslo - original_amount_of_chargers_in_oslo
        list_of_total_new_chargers.append(new_total_amount_of_chargers)
        total_amount_of_cost_implementing_those_chargers = new_total_amount_of_chargers * price_of_adding_new_charger
        list_of_new_costs_of_implementing_new_chargers.append(total_amount_of_cost_implementing_those_chargers)

        count_all_reneged_EVs = 0
        reneged_EVs_from_first_day = 0
        for cs in chargingstations.values():
            count_all_reneged_EVs += cs.reneged_EVs
            avg_reneged_EVs = count_all_reneged_EVs / total_amount_of_charging_stations
        list_of_total_reneged_EVs.append(count_all_reneged_EVs)
        list_of_avg_reneged_EVs.append(avg_reneged_EVs)
        reneged_EVs_from_first_day += list_of_total_reneged_EVs[0]

        count_all_rejected_EVs = 0
        rejected_EVs_from_first_day = 0
        for cs in chargingstations.values():
            count_all_rejected_EVs += cs.rejected_EVs
            avg_rejected_EVs = count_all_rejected_EVs / total_amount_of_charging_stations
        list_of_total_rejected_EVs.append(count_all_rejected_EVs)
        list_of_avg_rejected_EVs.append(avg_rejected_EVs)
        rejected_EVs_from_first_day += list_of_total_rejected_EVs[0]

        count_all_dejected_EVs = 0
        dejected_EVs_from_first_day = 0

        for cs in chargingstations.values():
            count_all_dejected_EVs += len(cs.dejected_EVs)
            avg_dejected_EVs = count_all_dejected_EVs / total_amount_of_charging_stations
        list_of_total_dejected_EVs.append(count_all_dejected_EVs)
        list_of_avg_dejected_EVs.append(avg_dejected_EVs)
        dejected_EVs_from_first_day += list_of_total_dejected_EVs[0]

        reduced_total_reneged_EVs_per_day = reneged_EVs_from_first_day - count_all_reneged_EVs
        reduced_total_rejected_EVs_per_day = rejected_EVs_from_first_day - count_all_rejected_EVs
        reduced_total_dejected_EVs_per_day = dejected_EVs_from_first_day - count_all_dejected_EVs
        list_of_difference_in_reduction_of_rejected_EVs.append(reduced_total_rejected_EVs_per_day)
        list_of_difference_in_reduction_of_reneged_EVs.append(reduced_total_reneged_EVs_per_day)
        list_of_difference_in_reduction_of_dejected_EVs.append(reduced_total_dejected_EVs_per_day)

        record_average_stats = namedtuple("average_stats", (
            'days', 'count_all_new_type1_chargers', 'count_all_new_type2_chargers', 'total_amount_of_extra_chargers',
            'total_amount_of_cost_implementing_those_chargers', 'total_reneged_EVs', 'total_rejected_EVs',
            'total_dejected_EVs', 'reduced_reneged_EVs', 'reduced_rejected_EVs', 'reduced_dejected_EVs',
            'count_all_chargers_in_oslo', 'avg_reneged_EVs',
            'avg_rejected_EVs', 'avg_dejected_EVs'))

        average_stats_per_day.append(record_average_stats(
            days=days,
            count_all_new_type1_chargers=count_all_new_type1_chargers,
            count_all_new_type2_chargers=count_all_new_type2_chargers,
            total_amount_of_extra_chargers=new_total_amount_of_chargers,
            total_amount_of_cost_implementing_those_chargers=total_amount_of_cost_implementing_those_chargers,
            total_reneged_EVs=count_all_reneged_EVs,
            total_rejected_EVs=count_all_rejected_EVs,
            total_dejected_EVs=count_all_dejected_EVs,
            reduced_reneged_EVs=reduced_total_reneged_EVs_per_day,
            reduced_rejected_EVs=reduced_total_rejected_EVs_per_day,
            reduced_dejected_EVs=reduced_total_dejected_EVs_per_day,
            count_all_chargers_in_oslo=count_all_chargers_in_oslo,
            avg_reneged_EVs=avg_reneged_EVs,
            avg_rejected_EVs=avg_rejected_EVs,
            avg_dejected_EVs=avg_dejected_EVs))

        """------------Call these methods to start information from 0 when new day start-------"""
        ElectricCars.wipe_out_all_EVs()
        for cs in chargingstations.values():
            cs.reset_method()
            #cs.add_3_more_chargers_of_type2_into_this_station()     # if you want to add new chargers to each station per day, it can be done here

except Exception as e:
    logger.exception(e)

avg_reneged_EVs = count_all_reneged_EVs / total_amount_of_charging_stations
avg_rejected_EVs = count_all_rejected_EVs / total_amount_of_charging_stations
avg_dejected_EVs = count_all_dejected_EVs / total_amount_of_charging_stations

print("Extra type1 chargers added: {} ".format(count_all_new_type1_chargers))
print("Extra type2 chargers added: {} ".format(count_all_new_type2_chargers))
print("Total extra chargers added :{}".format(new_total_amount_of_chargers) + " ---> Total cost is: {}".format(
    total_amount_of_cost_implementing_those_chargers))
print("Total chargers in Oslo are now: {} ".format(count_all_chargers_in_oslo))
print("-----------------------------------------------------------------------")
print("Total Reneged EVs are {} ".format(count_all_reneged_EVs))
print("Total Rejected EVs are {} ".format(count_all_rejected_EVs))
print("Total Exhausted EVs are: {} ".format(count_all_dejected_EVs))
print("---------------------------------------------------------------------")
print("Average Reneged EVs {} ".format(avg_reneged_EVs))
print("Average Rejected EVs {} ".format(avg_rejected_EVs))
print("Average Exhausted EVs {} ".format(avg_dejected_EVs))
print("---------------------------------------------------------------------")

print(colored("DAYS: {} ".format(list_of_days), 'green'))
print()
print(colored("list of new total amount of type1 added {} ".format(list_of_total_new_type1_added), 'cyan'))
print(colored("list of new total amount of type2 added {} ".format(list_of_total_new_type2_added), 'cyan'))
print(colored("list of new total amount of chargers added {} ".format(list_of_total_new_chargers), 'blue'))
print(colored("list of new costs in NOK per day {} ".format(list_of_new_costs_of_implementing_new_chargers), 'red'))
print()
print("list of total amount of chargers in Oslo {} ".format(list_of_total_amount_of_chargers))
print()

print(colored("list of avg reneged EVs per day {} ".format(list_of_avg_reneged_EVs), 'green'))
print(colored("list of avg rejected EVs per day {} ".format(list_of_avg_rejected_EVs), 'green'))
print(colored("list of avg dejected EVs per day {} ".format(list_of_avg_dejected_EVs), 'green'))
print()
print(colored("list of total reneged EVs per day {} ".format(list_of_total_reneged_EVs), 'magenta'))
print(colored("list of total rejected EVs per day {} ".format(list_of_total_rejected_EVs), 'magenta'))
print(colored("list of total dejected EVs per day {} ".format(list_of_total_dejected_EVs), 'magenta'))
print()
print("list of reductions in total reneged EVs per day {}".format(list_of_difference_in_reduction_of_reneged_EVs))
print("list of reductions in total rejected EVs per day {}".format(list_of_difference_in_reduction_of_rejected_EVs))
print("list of reductions in total dejected EVs per day {}".format(list_of_difference_in_reduction_of_dejected_EVs))

with open("results_per_day/avg_stats.csv", "a", newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for add_average_daily_into_to_csv in average_stats_per_day:
        filewriter.writerow(add_average_daily_into_to_csv)

with open('results_per_hour/ALL_stations_together.csv', 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for stats_per_hours_about_hotspots in hotspot_districts_stats_per_hours:
        filewriter.writerow(stats_per_hours_about_hotspots)
    for stats_per_hours_about_working_districts in work_districts_stats_per_hours:
        filewriter.writerow(stats_per_hours_about_working_districts)
    for stats_per_hours_about_main_districts in main_districts_stats_per_hours:
        filewriter.writerow(stats_per_hours_about_main_districts)

with open("results_per_day/ALL_stations_together.csv", "a", newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for daily_infos_about_hotspots in hotspot_daily_infos:
        filewriter.writerow(daily_infos_about_hotspots)
    for daily_infos_about_work_districts in work_districts_daily_infos:
        filewriter.writerow(daily_infos_about_work_districts)
    for daily_infos_about_main_districts in main_districts_daily_infos:
        filewriter.writerow(daily_infos_about_main_districts)

with open("results_per_hour/hotspot_districts_results_from_simulation.csv", 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for print_hourly_info in hotspot_districts_stats_per_hours:
        filewriter.writerow(print_hourly_info)

with open("results_per_day/hotspot_districts_results_from_simulation.csv", 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=";", quoting=csv.QUOTE_MINIMAL)
    for print_end_of_the_day_info in hotspot_daily_infos:
        filewriter.writerow(print_end_of_the_day_info)

with open('results_per_hour/work_districts_results_from_simulation.csv', 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for print_hourly_info in work_districts_stats_per_hours:
        filewriter.writerow(print_hourly_info)

with open('results_per_day/work_districts_results_from_simulation.csv', 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for print_end_of_the_day_info in work_districts_daily_infos:
        filewriter.writerow(print_end_of_the_day_info)

with open('results_per_hour/main_districts_results_from_simulation.csv', 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for print_hourly_info in main_districts_stats_per_hours:
        filewriter.writerow(print_hourly_info)

with open('results_per_day/main_districts_results_from_simulation.csv', 'a', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    for print_end_of_the_day_info in main_districts_daily_infos:
        filewriter.writerow(print_end_of_the_day_info)


