from random import randint
from termcolor import colored

from district_connections import collection_of_all_chargingstations_in_hotspot_districts, \
    collection_of_all_chargingstations_in_main_districts, collection_of_all_chargingstations_in_working_districts

total_amount_of_EVs_that_are_driving_to_work_districts = 26928
total_amount_of_EVs_that_are_driving_to_main_districts = 21528
total_amount_of_EVs_that_are_driving_to_hotspot_districts = 5376

EVs_per_hour_to_hotspot_districts = None
EVs_per_hour_to_work_districts = None
EVs_per_hour_to_main_districts = None


def generate_new_data():
    create_setup_lists()
    generate_work_districts_distribution()
    generate_hotspot_distributions()
    generate_distributions_to_main()


def distribute_faster(EVs, totals):
    counts = {total: 0 for total in range(totals)}
    while EVs > 0:
        adjust = EVs > 5000  # totally arbitrary
        change = 1
        if adjust:
            change = 5
        choice = randint(0, totals - 1)
        counts[choice] += change
        EVs -= change
    return counts


def create_setup_lists():
    "----Work districts setup----"
    global distributed_EVs_to_work
    distributed_EVs_to_work = []

    global EVs_per_hour_to_work_districts
    EVs_per_hour_to_work_districts = []

    total_work_districts = []

    all_working_districts_used = []

    all_working_districts_used.extend(collection_of_all_chargingstations_in_working_districts)

    for work_districts in all_working_districts_used:
        total_work_districts.append(work_districts)

    "----Main districts setup----"
    global distributed_EVs_to_main
    distributed_EVs_to_main = []

    global EVs_per_hour_to_main_districts
    EVs_per_hour_to_main_districts = []

    total_main_districts = []

    all_the_main_districts = []

    all_the_main_districts.extend(collection_of_all_chargingstations_in_main_districts)

    for main_districts in all_the_main_districts:
        total_main_districts.append(main_districts)

    "----Hotspot district setup----"
    global distributed_EVs_to_hotspot
    distributed_EVs_to_hotspot = []

    global EVs_per_hour_to_hotspot_districts
    EVs_per_hour_to_hotspot_districts = []

    total_hotspot_districts = []
    all_the_hotspot_districts = []

    all_the_hotspot_districts.extend(collection_of_all_chargingstations_in_hotspot_districts)

    for hotspot_districts in all_the_hotspot_districts:
        total_hotspot_districts.append(hotspot_districts)

    global five_hotspot_districts
    five_hotspot_districts = len(total_hotspot_districts)

    global eight_working_districts
    eight_working_districts = len(total_work_districts)

    global forty_seven_main_districts
    forty_seven_main_districts = len(total_main_districts)


def generate_work_districts_distribution():
    random_distribution_to_work = distribute_faster(total_amount_of_EVs_that_are_driving_to_work_districts,
                                                    eight_working_districts)
    print("Amount of EVs distributed to work stations: {}".format(random_distribution_to_work))

    for key, value in random_distribution_to_work.items():
        distributed_EVs_to_work.append(value)

    (work_districts) = (DistributionOfEVs('work', [distributed_EVs_to_work for x in range(num_EVs)]) for num_EVs in
                        distributed_EVs_to_work)

    for randomWork in work_districts:
        EVs_per_hour_to_work_districts.append([len(c) for c in randomWork.EVs_per_hour.values()])


def generate_distributions_to_main():
    random_distribution_to_main = distribute_faster(total_amount_of_EVs_that_are_driving_to_main_districts,
                                                    forty_seven_main_districts)
    # print("Amount of EVs distributed to main stations: {}".format(random_distribution_to_main))

    for key, value in random_distribution_to_main.items():
        distributed_EVs_to_main.append(value)

    (main_districts) = (DistributionOfEVs('main', [distributed_EVs_to_main for x in range(num_EVs)]) for num_EVs in
                        distributed_EVs_to_main)

    for randomMain in main_districts:
        EVs_per_hour_to_main_districts.append([len(c) for c in randomMain.EVs_per_hour.values()])


def generate_hotspot_distributions():
    random_distribution_to_hotspot = distribute_faster(total_amount_of_EVs_that_are_driving_to_hotspot_districts,five_hotspot_districts)
    print("Amount of EVs distributed to hotspot stations: {}".format(random_distribution_to_hotspot))

    for key, value in random_distribution_to_hotspot.items():
        distributed_EVs_to_hotspot.append(value)

    (hotspot_districts) = (DistributionOfEVs('hotspot', [distributed_EVs_to_hotspot for x in range(num_EVs)]) for num_EVs in distributed_EVs_to_hotspot)

    for randomHotspot in hotspot_districts:
        EVs_per_hour_to_hotspot_districts.append([len(c) for c in randomHotspot.EVs_per_hour.values()])


def show_amount_of_EVs_distributed_to_working_districts():
    print("DISTRIBUTION TO WORK:")
    how_many_EVs_was_sent_to_working_districts = []
    for if_i_want_to_see_EVs_per_hour_to_working_districts in EVs_per_hour_to_work_districts:
        how_many_EVs_was_sent_to_working_districts.append(sum(if_i_want_to_see_EVs_per_hour_to_working_districts))
        print(if_i_want_to_see_EVs_per_hour_to_working_districts)
        print("Total amount of EVs distributed to this specific station {}".format(
            sum(if_i_want_to_see_EVs_per_hour_to_working_districts)))
        print()
    print(colored("Total amount of EVs distributed to working stations : {} ".format(
        sum(how_many_EVs_was_sent_to_working_districts)),'red'))


def show_amount_of_EVs_distributed_to_hotspot_districts():
    print("DISTRIBUTION TO HOTSPOT:")
    how_many_EVs_was_sent_to_hotspot_districts = []
    for if_i_want_to_see_EVs_per_hour_hotspot_to_districts in EVs_per_hour_to_hotspot_districts:
        how_many_EVs_was_sent_to_hotspot_districts.append(sum(if_i_want_to_see_EVs_per_hour_hotspot_to_districts))
        print(if_i_want_to_see_EVs_per_hour_hotspot_to_districts)
        print("Total amount of EVs distributed to this specific station {}".format(
            sum(if_i_want_to_see_EVs_per_hour_hotspot_to_districts)))
        print()
    print(colored("Total amount of EVs distributed to hotspot stations : {} ".format(
        sum(how_many_EVs_was_sent_to_hotspot_districts)),'green'))


def show_amount_of_EVs_distributed_to_main_districts():
    print("DISTRIBUTION TO MAIN:")
    how_many_EVs_was_sent_to_main_districts = []
    for if_i_want_to_see_EVs_per_hour_to_main_districts in EVs_per_hour_to_main_districts:
        how_many_EVs_was_sent_to_main_districts.append(sum(if_i_want_to_see_EVs_per_hour_to_main_districts))
        print(if_i_want_to_see_EVs_per_hour_to_main_districts)
        print("Total amount of EVs distributed to this specific station {}".format(
            sum(if_i_want_to_see_EVs_per_hour_to_main_districts)))
        print()
    print(colored("Total amount of EVs distributed to main stations : {} ".format(sum(how_many_EVs_was_sent_to_main_districts)),'blue'))


class DistributionOfEVs:
    all_probabilities = {
        "work":
        # Hour:0      1      2     3      4     5      6      7      8      9     10     11    12    13     14     15    16    17    18     19    20     21      22     23      24
            (0.0, 0.0, 0.0, 0.0, 0.01, 0.05, 0.11, 0.19, 0.23, 0.2, 0.14, 0.05, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0),

        "main":
        # Hour:0      1      2     3      4     5      6      7      8      9     10     11    12    13     14     15    16    17    18     19    20     21      22     23      24
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.01, 0.02, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.08, 0.08,
             0.08, 0.07, 0.06, 0.06, 0.05, 0.04),

        "hotspot":
        # Hour:0      1      2     3      4     5      6      7      8      9     10     11    12    13     14     15    16    17    18     19    20     21      22     23      24
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.02, 0.03, 0.04, 0.08, 0.1, 0.12, 0.13, 0.13, 0.11,
             0.07, 0.05, 0.04, 0.03, 0.02, 0.01)

    }

    def __init__(self, type, EVs):
        total_length = len(EVs)
        self.EVs_per_hour = {}
        for hour, p in enumerate(self.all_probabilities[type]):
            try:
                self.EVs_per_hour[hour] = [EVs.pop() for i in range(int(p * total_length))]
            except IndexError:
                print('EVs already exhausted at hour {}'.format(hour))
        if EVs:
            print('{} left after distributing is finished'.format(len(EVs)))

